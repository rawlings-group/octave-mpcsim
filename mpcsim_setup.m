function mpcsim_setup(ifmissing)
% mpcsim_setup()
%
% Checks to make sure all packages needed to run MPCSim.
narginchk(0, 1);
if nargin() < 1
    ifmissing = 'warn';
end
successful = true();

% Get directory and subdirectory of this script.
thisdir = fileparts(mfilename('fullpath'));
subdir = fileparts(thisdir);
disp(subdir)
% Choose what to do on a missing package.
switch ifmissing
case 'error'
    missing = @error;
case 'warn'
    missing = @warning;
case 'ignore'
    missing = @(varargin) [];
otherwise
    error('Invalid argument!');
end

% Look for packages and add to path.
packages = {'casadi', 'mpctools'};
for i = 1:length(packages)
    package = packages{i};
    directory = [subdir, '/', package];
    if exist(directory, 'dir')
        fprintf('Found %s at\n\n%s\n\nAdding to path.\n\n', package, directory);
        addpath(directory);
    end
end

% Check to make sure MPCSim is in the right place.
thisdir = fileparts(mfilename('fullpath'));
if ~exist([thisdir, '/+mpcsim'], 'dir') ...
        || ~exist([thisdir, '/+mpcsim/SimCon.m'], 'file')
    missing(['MPCSim not found. Download from %s and unzip to a folder ', ...
             'called ''mpcsim''.'], bitbucket('octave-mpcsim'));
    successful = false();
end
fprintf('Adding mpcsim directory\n\n%s\n\nto path.\n\n', thisdir);
addpath(thisdir);

% Look for MPCTools and Casadi.
if isempty(which('casadiMEX'))
    missing(['Casadi not found. Download from files.casadi.org and unzip ' ...
             'to a folder called ''casadi''.']);
    successful = false();
end

if isempty(which('import_mpctools'))
    missing(['MPCTools not found. Download from %s and unzip to a folder ', ...
             'called ''mpctools''.'], bitbucket('octave-mpctools'));
    successful = false();
end

% Report success.
if successful
    curdir = pwd();
    if isequal(thisdir(1:min(length(curdir), length(thisdir))), curdir)
        thisdir = thisdir((length(curdir) + 1):end);
        if isempty(thisdir)
            thisdir = '.';
        end
    end
    fprintf('All packages found. Example scripts can be found in\n\n%s\n\n', ...
            [thisdir, '/examples']);
else
    error(['One or more packages not found! Double-check installation ', ...
           'instructions.']);
end

end%function

function l = bitbucket(package)
    % Returns bitbucket download link for a rawlings-group package.
    l = sprintf('bitbucket.org/rawlings-group/%s/downloads', package);
end%function

