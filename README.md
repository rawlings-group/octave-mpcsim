# MPCSim for Octave/Matlab #

This repo contains Octave/Matlab code to demonstrate MPC running in real time
on various model systems. In a single figure window, the inputs, states,
outputs, and disturbances of a system are shown in real time. MPC calculations
are performed using [MPCTools][mpctools] via [CasADi][casadi].

## Installation ##

Use of MPCSim requires four components:

1. A recent version of [GNU Octave](https://octave.org) (>= 4.0; older
   versions will *not* work) or Matlab (>= R2015b; older versions will likely
   work but have not been tested).

    * Examples will often make use of the `dlqr` and `dlqe` functions from
      the `control` package (Octave) and Control Toolbox (Matlab), so you will
      need to install these as well.
    
    * After installing Octave/Matlab, choose a folder where you will install the
      remaining packages, e.g., `~/octave` or `Documents/MATLAB`. We will refer
      to this folder as `installdir` for the rest of the instructions.

2. [CasADi][casadi] for Octave or Matlab (>= 3.0; older versions will *not*
   work).

    * Open the [CasADi installation instructions][casadiinstall] and choose the
      appropriate version for your OS Octave/Matlab version.

    * In the `installdir` folder, create a new folder called `casadi`. Unzip
      all of the CasADi files to that folder.

3. [MPCTools][mpctools] for Octave/Matlab

    * Download `mpctools.zip` from the [MPCTools Downloads page][mpctoolsdl]
      and unzip into `installdir`. You should now have a folder called
      `installdir/mpctools`.

4. MPCSim (this repository)

    * Download `mpcsim.zip` from the [MPCSim Downloads page][mpcsimdl] and
    unzip into `installdir`. You should now have a folder called
    `installdir/mpcsim`.

To check that everything is installed correctly, open Octave/Matlab, navigate
to the `installdir/mpcsim` folder and run `mpcsim_setup()` in the interpreter. This script will add all of the necessary folders to the Octave/Matlab path.

Note that if you already have CasADi and/or MPCTools installed in different
locations, you can manually add those folders to the path via

```
addpath('/path/to/casadi')
addpath('/path/to/mpctools')
```

Running `mpcsim_setup()` will check to make sure that these packages are
visible to Octave/Matlab.

## Running Scripts ##

Once the `mpcsim_setup` script reports successful completion, you can run the
example scripts (stored in `mpcsim/examples`) as you would any other
Octave/Matlab script. Note that by default, the `mpcsim/examples` folder is
*not* added to the Octave/Matlab path, so you will need to open that folder
from within Octave/Matlat, or manually add it to the path via

```
addpath('/path/to/mpcsim/examples')
```

Running an example script will produce a figure window with menus and buttons
for interaction.

One some systems the octave default graphics package (qt) does not
perform well, causing the main GUI window to be all black or to
otherwise misbehave.  If this happens try using the .octaverc file
supplied with this package.  Simply copy this file to your home
directory and restart octave. This issues the octave command 
graphics_toolkit('fltk')


## Support ##

MPCSim is intended to used in guided demonstrations only, and thus it is
largely undocumented. If you encounter bugs as an end user, please open an
issue on the [MPCSim issue tracker][mpcsimissues].

Documentation for the user interface may be added at some point, but there are 
no plans to document the actual MPCSim code, as the implementation of MPCSim is
subject to change. Users who wish to simulate their own systems via MPCSim are
welcome to modify one of the example scripts, but support will not be provided.

[mpctools]: https://bitbucket.org/rawlings-group/octave-mpctools
[mpctoolsdl]: https://bitbucket.org/rawlings-group/octave-mpctools/downloads/
[mpcsimdl]: https://bitbucket.org/rawlings-group/octave-mpcsim/downloads/
[mpcsimissues]: https://bitbucket.org/rawlings-group/octave-mpcsim/issues
[casadi]: https://casadi.org
[casadiinstall]: https://github.com/casadi/casadi/wiki/InstallationInstructions
