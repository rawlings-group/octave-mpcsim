# Makefile for zip distribution.
PKG := +mpcsim
MAKEZIP := util/makezip.py
MPCSIM_SRC := $(shell hg status -acmn $(PKG)*) $(PKG)/icons.mat mpcsim_setup.m

# List example scripts here.
EXAMPLES_SRC := $(addprefix examples/, hotairballoon.m hab_open_loop.m cstr_mpcsim.m cstr_mpcsim_zoomout.m examplegui.m)

DIST_NAME := mpcsim.zip

$(DIST_NAME) : $(MPCSIM_SRC) $(EXAMPLES_SRC) README.md
	@echo Making $@
	@$(MAKEZIP) --root mpcsim --name $(DIST_NAME) $^

dist : $(DIST_NAME)
.PHONY : dist

ICON_SRC := $(wildcard icons/*.bmp)

$(PKG)/icons.mat : util/bmp2mat.py $(ICON_SRC)
	@echo Making $@.
	@python $^ --output $@

dist : $(DIST_NAME)
.PHONY : dist

# Rule for Bitbucket upload.
UPLOAD_DIR := https://api.bitbucket.org/2.0/repositories/rawlings-group/octave-mpcsim/downloads
upload : $(DIST_NAME)
	echo -n "Enter bitbucket username: " && read bitbucketuser && curl -v -u $$bitbucketuser $(UPLOAD_DIR) -F files=@"$<"
.PHONY : upload

