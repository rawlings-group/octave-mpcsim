function val = askval(varargin)
% val = askval(prompt, ...)
%
% Asks for a value using a dialog box.
%
% Optional keyword arguments are as follows:
%
% * min, max : Float
%
%   > Define minimum and maximum limits for the value.
%
% * current : Float
%
%   > Gives current value (displayed for user's reference).
%
% * type : String ['float']
%
%   > String specifying type of value. Can be 'float', 'int', or 'bool'.
%
% * limformat : String ['%g']
%
%   > printf formatting string to use to display the current limits
%
% * showlims : Bool [True]
%
%   > Whether to show the limits to the user. Note that even if this is False,
%   any provided limits are still enforced.
%
% * title : String ['']
%
%  > String to use as window title.
%
% * terminal : Bool [False]
%
%  > Whether to display text in the terminal window instead of as a popup
%  window.
%
% Returns empty matrix if user cancels or enters nothing.
persistent parser
if isempty(parser)
    parser = mpctools.ArgumentParser();
    parser.add('prompt', 'required', 'str');
    parser.add('min', [], {'numeric', 'scalar'});
    parser.add('max', [], {'numeric', 'scalar'});
    parser.add('current', [], {'scalar'});
    parser.add('type', 'float', 'str');
    parser.add('limformat', [], 'str');
    parser.add('showlims', true(), {'scalar', 'bool'});
    parser.add('title', '', 'str');
    parser.add('terminal', false(), {'scalar', 'bool'});
end
if nargin() == 1 && ischar(varargin{1})
    varargin{1} = {varargin{1}};
end 
args = parser.parse(varargin{:});
if ~isnumeric(args.current) && ~islogical(args.current)
    error('Argument current must be numeric!');
end

% Set some values based on type.
switch args.type
case {'float', 'double'}
    if isempty(args.limformat)
        args.limformat = '%g';
    end
case 'int'
    if isempty(args.limformat)
        args.limformat = '%d';
    end
case 'bool'
    args.limformat = '%d';
    args.min = [];
    args.max = [];
otherwise
    error('Invalid type specification: "%s"!', args.type);
end

% Construct input string.
if args.showlims
    messages = repmat({''}, 3, 1);
    data = {'Currently', args.current; 'Min', args.min; 'Max', args.max};
    for i = 1:size(data, 1)
        if ~isempty(data{i,2})
            messages{i} = sprintf(['\n    %s: ', args.limformat], ...
                                  data{i,:});
        end        
    end
    args.prompt = [args.prompt, messages{:}];
end

% Add default limits.
if isempty(args.min)
    args.min = -inf();
end
if isempty(args.max)
    args.max = inf();
end

% Run dialog.
if isequal(args.type, 'bool')
    val = askbool(args);
else
    % Need to loop this one because user could provide invalid input.
    okay = false();
    while ~okay
        [val, okay] = askfloat(args);
    end
end

end%function


% *****************************************************************************

function [val, okay] = askfloat(args)
    % Ask for a floating-point value with bounds checking, etc.
    okay = false();
    
    % Make dialog box.
    if args.terminal
        response = inputdlg_terminal(args.prompt, args.title);
    else
        response = inputdlg(args.prompt, args.title);
    end
    
    % Return empty matrix if user cancelled. Otherwise, validate.
    if isempty(response)
        val = [];
        okay = true();
    else
        str = response{1};
        if isempty(str)
            val = [];
            okay = true();
        else   
            val = str2double(str);
            errmsg = '';
            if isnan(val)
                errmsg = sprintf('Not a valid number: %s', str);
            elseif val < args.min
                errmsg = 'Value is to small!';
            elseif val > args.max
                errmsg = 'Value is too large!';
            elseif isequal(args.type, 'int') && round(val) ~= val
                errmsg = 'Value must be an integer!';
            end
            if ~isempty(errmsg)
                errordlg(errmsg);
            else
                okay = true();
            end
        end
    end
end%function

function val = askbool(args)
    % Ask for a boolean value.
    NO = '0 (False)';
    YES = '1 (True)';
    CANCEL = 'Cancel';
    if args.terminal
        response = questdlg_terminal(args.prompt, args.title, NO, YES, CANCEL);
    else
        response = questdlg(args.prompt, args.title, NO, YES, CANCEL, CANCEL);
    end
    switch response
    case NO
        val = false();
    case YES
        val = true();
    otherwise
        val = [];
    end 
end%function

function str = inputdlg_terminal(prompt, titlestr)
    % Emulates inputdlg but in a terminal.
    narginchk(2, 2);
    if ~isempty(titlestr)
        fprintf('*** %s ***\n', titlestr);
    end
    str = {input([prompt, sprintf('\nEnter value: ')], 's')};
end%function

function str = questdlg_terminal(prompt, titlestr, no, yes, cancel)
    % Emulates questdlg but in a terminal.
    narginchk(5, 5);
    if ~isempty(titlestr)
        fprintf('*** %s ***\n', titlestr);
    end
    fprintf([prompt, '\n']);
    valid = false();
    while ~valid
        val = input(sprintf('[%s / %s / %s]: ', yes, no, cancel), 's');
        switch lower(val)
        case {'1', 'true', 'yes', 'y'}
            str = yes;
            valid = true();
        case {'0', 'false', 'no', 'n'}
            str = no;
            valid = true();
        case {'cancel', 'c', ''}
            str = cancel;
            valid = true();
        otherwise
            fprintf('Invalid choice!\n');
        end
    end
end%function

