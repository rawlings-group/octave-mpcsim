function tight_layout(fig, subplots, bbox, pad)
% tight_layout(fig, subplots, [bbox], [pad])
%
% Arranges the subplots on the current figure to be tight.
%
% subplots should be a cell array of the axes handles. Rect should be a 4-
% vector giving the total bbox to use for all the axes. Pad should be a scalar
% or 2-vector to specify how much extra padding (in points) to add.
narginchk(2, 4);

if nargin() < 3
    bbox = [0, 0, 1, 1];
elseif numel(bbox) ~= 4
    error('bbox must be a vector of length 4!');
end

if nargin() < 4
    pad = 2*[1, 1]*get(subplots{1, 1}, 'fontsize');
elseif isscalar(pad)
    pad = [pad, pad];
elseif numel(pad) ~= 2
    error('pad must be a scalar or 2-vector!');
end
pad = max(pad, 0);    

sizes = cell(size(subplots));

% Get available size for subplots.
Nrows = size(subplots, 1);
Ncols = size(subplots, 2);
axwidth = (bbox(3) - bbox(1))/Ncols;
axheight = (bbox(4) - bbox(2))/Nrows;

% Determine 1 point in figure units.
set(fig, 'units', 'inches');
figpts = 72*get(fig, 'position');
pad = pad./figpts([3, 4]); % Convert from points to figure units.

% Determine padding for each axis.
xpad = zeros(size(subplots));
ypad = zeros(size(subplots));
for i = 1:Nrows
    for j = 1:Ncols
        if ~isempty(subplots{i, j});
            axsize = get(subplots{i, j}, 'position');
            axpad = getaxlabelpad(subplots{i, j});
            
            xpad(i, j) = axpad.ylabel*axsize(3);
            ypad(i, j) = axpad.xlabel*axsize(4);
        end
    end
end
xpad = max(xpad, [], 1);
ypad = max(ypad, [], 2);

% Set positions.
for i = 1:Nrows
    for j = 1:Ncols
        if ~isempty(subplots{i, j})
            pos = [bbox(1) + (j - 1)*axwidth + xpad(j) + pad(1)/2, ...
                   bbox(2) + (Nrows - i)*axheight + ypad(i) + pad(2)/2, ...
                   axwidth - xpad(j) - pad(1), ...
                   axheight - ypad(i) - pad(2)];
            set(subplots{i, j}, 'position', pos);
        end
    end
end

end%function

function pad = getaxlabelpad(ax)
    % Returns a struct with fields xlabel and ylabel giving the appropriate
    % padding for each label.
    pad = struct();
    xlims = get(ax, 'xlim');
    ylims = get(ax, 'ylim');
    
    axnames = 'yx'; % Needs to be in this order.
    axoffset = [xlims(1), ylims(1)];
    axscale = [diff(xlims), diff(ylims)];
    
    for i = 1:length(axnames)
        s = axnames(i);
        extent = get(get(ax, [s, 'label']), 'extent'); % In data units.
        p = (axoffset(i) - extent(i))/axscale(i); % In axes units.
        pad.([s, 'label']) = max(p, 0);
    end
end%function
