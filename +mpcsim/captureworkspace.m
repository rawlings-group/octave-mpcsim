function ws = captureworkspace(varargin)
% ws = captureworkspace([omit1], [omit2], ...);
%
% Captures the workspace of the calling function and returns it as a struct.
%
% Any names provided will be omitted from the struct.
varnames = setdiff(evalin('caller', 'who()'), varargin);
ws = struct();
for i = 1:length(varnames)
    v = varnames{i};
    ws.(v) = evalin('caller', v);
end
end%function

