classdef Button < handle

properties (GetAccess = public, SetAccess = private)
    label;
    handle;
    copies = zeros(0, 1);
    callbackfunc;
    state;
    btype;
    istoggle;
end%properties

properties (GetAccess = private, SetAccess = private)
    buttoncb;
    icon = [];
end%properties

methods (Access = public)
    function self = Button(parent, label_, callbackfunc_, istoggle_, icon_)
        % Button(parent, label, callback, istoggle, [icon])
        %
        % Container class for a generic button.
        %
        % If parent is a toolbar handle, the button is added as a button using
        % The given icon. Otherwise, it is added as a menubar.
        narginchk(4, 5);
        if ~ishandle(parent)
            error('parent must be a handle!');
        end
        if ~ischar(label_)
            error('label must be a string!');
        end
        self.label = label_;
        if isequal(get(parent, 'type'), 'uitoolbar')
            self.btype = 'button';
        else
            self.btype = 'menu';
        end
        self.istoggle = istoggle_;
        if isempty(callbackfunc_)
            self.callbackfunc = @(varargin) [];
        else
            self.callbackfunc = callbackfunc_;
        end
        
        % Create button.
        self.state = false();
        self.buttoncb = mpcsim.func2callback(@() self.press());
        if nargin() >= 5
            self.icon = icon_;
        end
        self.handle = self.makebutton(parent);
        if self.istoggle
            self.toggle(self.state, false());
        end
    end%function
    
    function copyto(self, parent)
        % Make a copy of the button on a different parent.
        self.copies = [self.copies; self.makebutton(parent)];
        if self.istoggle
            self.toggle(self.state, false());
        end
    end%function
    
    function press(self)
        % Activates a button press.
        if self.istoggle
            switch self.btype
            case 'button'
                onoff = isequal(get(self.handle, 'state'), 'on');
            case 'menu'
                onoff = ~self.state;
            end
            self.toggle(onoff);
        else
            switch self.btype
            case 'button'
                if isequal(get(self.handle, 'state'), 'on')
                    self.callbackfunc();
                    for h = [self.handle; self.copies]
                        set(h, 'state', 'off');
                    end
                end
            case 'menu'
                self.callbackfunc();
            end
        end
    end%function
    
    function toggle(self, onoff, runcallback)
        % self.toggle(onoff, [runcallback=true])
        %
        % Toggles the value of the button. Errors if button is not a toggle.
        narginchk(2, 3);
        if nargin() < 3
            runcallback = true();
        end
        if ~self.istoggle
            error('This Button is not a toggle! Cannot toggle()!');
        end
        self.state = onoff;
        if self.state
            statestr = 'ON';
        else
            statestr = 'off';
        end
        for h = [self.handle; self.copies]
            switch self.btype
            case 'button'
                set(h, 'state', statestr);
            case 'menu'
                set(h, 'label', sprintf('[%s: %s]', self.label, statestr));
            end
        end
        if runcallback
            self.callbackfunc(self.state);
        end
    end%function
    
    function disable(self)
        % Disables button.
        self.disable_or_enable(true());
    end%function
    
    function enable(self)
        % Enables button.
        self.disable_or_enable(false());
    end%function
    
    function disable_or_enable(self, disable)
        % self.disable_or_enable_button(button, disable)
        %
        % disable is true to disable button or false to enable.
        narginchk(2, 2);
        if disable
            onoff = 'off';
            color = [0, 0, 0];
        else
            onoff = 'on';
            color = 'black';
        end
        for h = [self.handle; self.copies]
            set(h, 'enable', onoff);
            
            switch self.btype
            case 'menu'
                set(h, 'foregroundcolor', color);
            end
        end
    end%function
end%methods

methods (Access = private)
    function h = makebutton(self, parent)
        % Make a button with the given parent.
        switch self.btype
        case 'button'
            h = uitoggletool(parent, 'TooltipString', self.label, ...
                             'ClickedCallback', self.buttoncb, ...
                             'CData', self.icon);
        case 'menu'
            h = uimenu(parent, 'callback', self.buttoncb, ...
                       'label', sprintf('[%s]', self.label));
        end
    end%function
end%methods

end%classdef

