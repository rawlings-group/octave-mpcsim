classdef VectorQueue < handle
% Simple queue for numeric vectors (stored internally as a matrix).

properties (GetAccess=private, SetAccess=private)
    data_; % Matrix holding the data.
    init_data_; % Initial data that was given.
    jfront;
    jback;
    maxlength; % Maximum length of currently allocated data.
    nrows;
end%properties

methods
    function self = VectorQueue(data)
        % VectorQueue(data)
        %
        % Initialize the object from a given matrix.
        if ~ismatrix(data)
            error('data must be a matrix!');
        end
        self.init_data_ = data;
        self.reset();
    end%function
    
    function reset(self)
        % Resets the object to its state at creation time.
        data = self.init_data_;
        self.nrows = size(data, 1);
        self.data_ = data;
        self.jfront = 1;
        self.jback = size(data, 2);
        self.resize();
    end%function
    
    function resize(self)
        % Resize the object, eliminating popped data and adding extra space for
        % new elements.
        ncols = max(self.length(), 1);
        if iscell(self.data_)
            pad = cell(self.nrows, ncols);
        else
            pad = NaN(self.nrows, ncols);
        end
        self.data_ = [self.get(), pad];
        self.jfront = 1;
        self.jback = size(self.data_, 2) - size(pad, 2);
        self.maxlength = size(self.data_, 2);
    end%function
    
    function v = front(self, empty)
        % v = self.front([empty])
        %
        % Returns the vector at the front of the queue.
        %
        % If the queue is empty, returns `empty` if given; otherwise issues an
        % error.
        narginchk(1, 2);
        if self.isempty()
            if nargin() >= 2
                v = empty;
            else
                error('Queue is empty!');
            end
        else
            v = self.data_(:,self.jfront);
        end
    end%function
    
    function v = back(self, empty)
        % v = self.back([empty])
        %
        % Returns the vector at the back of the queue.
        %
        %
        % If the queue is empty, returns `empty` if given; otherwise issues an
        % error.
        narginchk(1, 2);
        if self.isempty()
            if nargin() >= 2
                v = empty;
            else
                error('Queue is empty!');
            end
        else
            v = self.data_(:,self.jback);
        end
    end%function
    
    function push(self, v)
        % self.push(v)
        %
        % Pushes the vector v to the back of the queue.
        v = v(:);
        if length(v) ~= self.nrows
            error('v must be length %d (is length %d)!', self.nrows, length(v));
        end
        if self.jback == self.maxlength
            self.resize();
        end
        self.jback = self.jback + 1;
        self.data_(:,self.jback) = v;
    end%function
    
    function v = pop(self)
        % v = self.pop()
        %
        % Pops the front-most vector from the queue and returns it.
        v = self.front();
        self.jfront = self.jfront + 1;
    end%function
    
    function vpop = pushpop(self, vpush)
        % vpop = self.pushpop(vpush)
        %
        % Pushes the given vector to the back of the queue and pops the vector
        % currently at the front of the queue.
        %
        % Note that the push occurs before the pop, which means this will have
        % no effect if the queue is currently empty.
        self.push(vpush);
        vpop = self.pop();
    end%function
    
    function data = get(self, j)
        % data = self.get([j])
        %
        % Returns the all of the data currently in the queue or the data at a
        % specific column j.
        %
        % Negative values of j index from the back.
        narginchk(1, 2);
        if nargin() < 2
            data = self.data_(:,self.jfront:self.jback);
        else
            if ~isscalar(j)
                error('j must be a scalar!');
            end
            j = round(j);
            if j > 0
                jdata = self.jfront - 1 + j;
                if jdata > self.jback
                    error('j = %d is outside of length %d!', j, self.length());
                end
            elseif j < 0
                jdata = self.jback + 1 + j;
                if jdata < self.jfront
                    error('j = %d is outside of length -%d!', j, self.length());
                end
            else
                error('j cannot be zero!');
            end
            data = self.data_(:,jdata);
        end
    end%function
    
    function l = length(self)
        % l = self.length()
        %
        % Returns the number of vectors currently in the queue.
        l = self.jback - self.jfront + 1;
    end%function
    
    function tf = isempty(self)
        % tf = self.isempty()
        %
        % Returns true or false whether the object is empty.
        tf = self.length() <= 0;
    end%function
    
    function s = size(self, dim)
        % s = self.size([dim])
        %
        % Returns the object's size, optionally in the given dimension.
        narginchk(1, 2);
        s = [self.nrows, self.length()];
        if nargin() >= 2
            switch dim
            case {1, 2}
                s = s(dim);
            otherwise
                error('Invalid value for dim! Must be 1 or 2!');
            end
        end
    end%function
end%methods

end%classdef

