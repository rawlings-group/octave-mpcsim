function clearmenus(fig)
% clearmenus(fig)
%
% Removes all of the uimenus from the given figure.
narginchk(1, 1);
if ~ishandle(fig) || ~isequal(get(fig, 'type'), 'figure')
    error('fig must be a figure handle!');
end

% Loop through handles and decide which ones to delete. We can't actually delete
% them yet, because deleting a parent invalidates the child.
handles = findall(fig);
todelete = false(size(handles));
for i = 1:length(handles)
    h = handles(i);
    htype = get(h, 'type');
    hparent = get(h, 'parent');
    if isequal(htype, 'uimenu') && isequal(hparent, fig)
        todelete(i) = true();
    end
end

% Now actually delete everybody.
for i = 1:length(handles)
    if todelete(i)
        delete(handles(i));
    end
end

end%function

