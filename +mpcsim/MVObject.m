
classdef MVObject < mpcsim.PlotVar
    
methods
    function self = MVObject(varargin)
        % self = MVObject(name, [label], [defaults],
        %                 'field1', val1, 'field2', val2, ...)
        %
        % Initializes the object.
        %
        % Defaults can be True to set defaults, False to not include any, or
        % it can be a cell array of strings to only use certain defaults.
        self = self@mpcsim.PlotVar(varargin{:});
        self.stairstep = true();
    end%function
    
    function initaxes(self, ax_)
        % Sets up the plot for the current object.
        narginchk(2, 2);
        initaxes@mpcsim.PlotVar(self, ax_);
        self.lines.setpoint.label = 'Target';
        self.lines.past.label = 'Measured';
    end%function
end%methods

methods (Static)
    function defaults = get_default_fields()
        % Returns a struct of default values for all of the object fields.
        % Note that this is the the class defaults, and not the values used for
        % initialization.
        defaults = mpcsim.PlotVar.get_default_fields();
        defaults.ssrvalue = {0, '.min', 0, '.name', 'SS Targ. R Weight (Rs)'};
        defaults.rvalue = {1, '.min', 0, '.name', 'MPC R Weight (Rc)'};
        defaults.svalue = {0, '.min', 0, '.name', 'MPC S Weight (Sc)'};
        defaults.roclim = {inf(), '.min', 0, '.name', 'Max Rate-of-Change'};
        defaults.dnoise = {1, '.min', 0, '.name', 'Disturbance Model Noise (Qed)'};
    end%function
    
    function required = get_required_fields()
        % Return required fields.
        required = mpcsim.PlotVar.get_required_fields();
        required.value = {0, '.min', 'min', '.max', 'max'}; % Not hidden.
    end%function
end%methods

end%classdef
