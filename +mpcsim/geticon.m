function [data, found] = geticon(name, N)
% [data, found] = geticon(name, N)
%
% Returns a N by M by 3 array of icon data based on the name. N defaults to
% 16 to be compatible with Matlab's uitoggletool objects.
%
% If the name is not known, the default icon is returned, and found is false.
% Otherwise, found is true.
persistent ICONS
if isempty(ICONS)
    ICONS = loadiconsmat();
end

narginchk(1, 2);
if nargin() < 2
    N = 16;
end

found = isfield(ICONS, name);
if found
    data = ICONS.(name);
    if size(data, 1) ~= N
        data = interpicon(data, N);
    end
else
    data = default_icon(N);
end

end%function

function data = default_icon(N, color)
    % Returns default icon.
    narginchk(1, 2);
    N = max(round(N), 1);
    if nargin() < 2
        color = [0, 0, 0];
    elseif numel(color) ~= 3
        error('color must be a 3-vector!');
    end
    
    i = linspace(-1, 1, N);
    [x, y] = meshgrid(i, i);
    
    mask = false(size(x));
    for i = [-1, 1]
        mask((y > i*x - 0.1) & (y < i*x + 0.1)) = true();
    end
    mask(abs(x) + abs(y) > 0.75) = false();
    
    data = cell(3, 1);
    for i = 1:3
        data{i} = ones(size(mask));
        data{i}(mask) = color(i);
    end
    data = cat(3, data{:});
end%function

function iicon = interpicon(icon, N)
    % Re-samples the given icon to be size N.
    [xi, yi] = meshgrid(linspace(0, 1, size(icon, 1)), ...
                                 linspace(0, 1, size(icon, 2)));
    [x, y] = meshgrid(linspace(0, 1, N), linspace(0, 1, N));
    iicon = zeros(N, N, 3);
    for i = 1:3
        iicon(:,:,i) = interp2(xi, yi, icon(:,:,i), x, y);
    end
end%function

function icons = loadiconsmat()
    % Attempt to load icons.mat from the directory of this file.
    thisdir = fileparts(mfilename('fullpath'));
    mat = [thisdir, '/icons.mat'];
    if exist(mat, 'file')
        icons = load(mat);
    else
        warning('%s not found! Icons will be missing.', mat);
        icons = struct();
    end
end%function

