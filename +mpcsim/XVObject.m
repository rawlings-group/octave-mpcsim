classdef XVObject < mpcsim.PlotVar

methods
    function self = XVObject(varargin)
        % self = XVObject(name, [label], [defaults],
        %                 'field1', val1, 'field2', val2, ...)
        %
        % Initializes the object.
        %
        % Defaults can be True to set defaults, False to not include any, or
        % it can be a cell array of strings to only use certain defaults.
        self = self@mpcsim.PlotVar(varargin{:});
    end%function
    
    function initaxes(self, ax_)
        % Sets up the plot for the current object.
        narginchk(2, 2);
        initaxes@mpcsim.PlotVar(self, ax_);
        self.lines.setpoint.label = 'Target';
        self.addline('estpast', 'Estimated', ...
                     (-self.Npast:0)*self.Delta, ...
                     self.get('estvalue')*ones(self.Npast + 1, 1), '--b');
        self.addline('mhe', 'Current MHE', NaN(), NaN(), '--c');
    end%function
    
    function updatelines(self, dostep)
        % updatelines(self, [dostep=True])
        %
        % Updates all of the given lines. If dostep is True, bounds and other
        % lines are updated by one step.
        narginchk(1, 2);
        if nargin() < 2
            dostep = true();
        end
        if dostep
            estpast = self.lines.estpast;
            estpast.y = [estpast.y(2:(self.Npast + 1)); self.get('estvalue')];
        end
        updatelines@mpcsim.PlotVar(self, dostep);
    end%function
end%methods

methods (Static)
    function defaults = get_default_fields()
        % Returns a struct of default values for all of the object fields.
        % Note that this is the the class defaults, and not the values used for
        % initialization.
        defaults = mpcsim.PlotVar.get_default_fields();
        defaults.mnoise = {0, '.min', 0, '.name', 'Estimator Model Noise (Qe)'};
    end%function
    
    function required = get_required_fields()
        % Returns required fields.
        required = mpcsim.PlotVar.get_required_fields();
        required.estvalue = {0, '.name', 'Est. Value', '.visible', false()};
        required.setpoint = {NaN(), '.name', 'Target', '.visible', false()};
    end%function
end%methods

end%classdef
