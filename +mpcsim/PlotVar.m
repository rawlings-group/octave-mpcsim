classdef PlotVar < mpcsim.Option
% PlotVar
%
% Object for holding information (options and plot data) about a variable.

properties
    lines = struct();
    default_lines = struct();
    ax = [];
    leg = [];
    label;
    Delta = 1;
    stairstep = false();
    Nfuture = 25;
    Npast = 100;
end%properties

methods
    function self = PlotVar(name_, label_, defaultfields, varargin)
        % self = PlotVar(name, label, defaultfields, 'field', value, ...)
        %
        % Initialize the object. Note that default fields cannot be excluded.
        % 'field', value pairs are specified as in mpcsim.Option.
        if nargin() < 1
            error('name is required!');
        end
        self = self@mpcsim.Option(name_);
        if nargin() < 2 || isempty(label_)
            label_ = name_;
        end
        self.label = label_;
        
        % Get default fields and initialize.
        if nargin() < 3
            defaultfields = true();
        end
        defaults = self.getdefaultlist(defaultfields);
        self.init(defaults{:}, varargin{:});
    end%function
    
    function tf = is_plotted(self)
        % Whether the variable is plotted or not.
        tf = ~isempty(self.ax);
    end%function
    
    function initaxes(self, ax_)
        % Sets up the plot for the current object.
        self.ax = ax_;
        hold(self.ax, 'on');
        box(self.ax, 'on');
        self.updateaxlims();
        ylabel(self.ax, self.label);
        
        % Divider, past, future, and setpoint.
        self.addline('past', 'Actual', (-self.Npast:0)*self.Delta, ...
                     self.get('value')*ones(self.Npast + 1, 1), '-k');
        self.addline('divider', '', [0; 0], ...
                     get(self.ax, 'ylim'), '-r');
        self.addline('future', 'Future Pred.', (0:self.Nfuture)*self.Delta, ...
                     self.get('value')*ones(self.Nfuture + 1, 1), '-g');
        self.addline('setpoint', 'Setpoint', (-self.Npast:self.Nfuture)*self.Delta, ...
                     self.get('setpoint')*ones(self.Npast + self.Nfuture ...
                                               + 1, 1), ':k');
        self.lines.setpoint.stairstep = true();
        
        % Min and max limits.
        x = (-self.Npast:self.Nfuture)*self.Delta;
        y = ones(size(x));
        self.addline('min', 'Min/Max Limits', x, self.get('min')*y, '--r');
        self.addline('max', '', x, self.get('max')*y, '--r');
        self.lines.min.stairstep = true();
        self.lines.max.stairstep = true();
    end%function
    
    function showlegend(self)
        % Creates the legend for the object.
        if ~isempty(self.leg)
            legend(self.ax, 'show');
        else
            linenames = fieldnames(self.lines);
            handles = zeros(size(linenames));
            labels = cell(size(linenames));
            keep = false(size(linenames));
            for i = 1:length(linenames)
                l = self.lines.(linenames{i});
                if ~isempty(l.label)
                    handles(i) = l.line;
                    labels{i} = l.label;
                    keep(i) = true();
                end
            end
            handles = handles(keep);
            labels = labels(keep);
            self.leg = legend(self.ax, handles, labels, ...
                              'location', 'NorthWest');
            if ~mpctools.isOctave()
                % Disables some legend calculations in Matlab.
                setappdata(self.ax, 'LegendColorbarManualSpace', 1);
                setappdata(self.ax, 'LegendColorbarReclaimSpace', 1);
            end
        end
    end%function
    
    function hidelegend(self)
        % Deletes the legend for the object.
        if ~isempty(self.leg)
            delete(self.leg);
        end
        self.leg = [];
    end
    
    function updatelines(self, dostep)
        % updatelines(self, [dostep=True])
        %
        % Updates all of the given lines. If dostep is True, bounds and other
        % lines are updated by one step.
        narginchk(1, 2);
        if nargin() < 2
            dostep = true();
        end
        if dostep
            self.lines.min.y = [self.lines.min.y(2:(self.Npast + 1)); ...
                                self.get('min')*ones(self.Nfuture + 1, 1)];
            self.lines.max.y = [self.lines.max.y(2:(self.Npast + 1)); ...
                                self.get('max')*ones(self.Nfuture + 1, 1)];
            self.lines.past.y = [self.lines.past.y(2:(self.Npast + 1)); ...
                                 self.get('value')];
            self.lines.setpoint.y = [self.lines.setpoint.y(2:(self.Npast + 1));
                                     self.get('setpoint')*ones(self.Nfuture + 1, 1)];
        end
        linenames = fieldnames(self.lines);
        for i = 1:length(linenames)
            self.lines.(linenames{i}).update();
        end
        self.updateaxlims();
    end%function
    
    function addline(self, name, label, x, y, varargin)
        % self.addline(name, label, x, y, [style], ...)
        %
        % Adds a line to the current object.
        %
        % name must be a valid fieldname. label is the label to use in the
        % legend (empty string to not show). x and y must be vectors of data.
        % All remaining arguments are passed to plot().
        narginchk(5, inf());
        self.lines.(name) = mpcsim.PlotLine(self.ax, x, y, varargin, ...
                                            self.stairstep, label);
        self.lines.(name).update();
    end%function
    
    function removeline(self, name)
        % Removes the given line by name.
        if ~isfield(self.lines, name)
            error('Unknown line %s!', name);
        end
        self.lines.(name).delete();
        self.lines = rmfield(self.lines, name);
    end%function
    
    function togglelegend(self, on)
        % self.togglelegend([true/false])
        %
        % Toggles or sets the current state of the legend. true to show, false
        % to hide, and no argument to toggle.
        if nargin() < 2
            on = isempty(self.leg);
        end
        if on
            self.showlegend();
        else
            self.hidelegend();
        end
    end%function
    
    function savedefaults(self)
        % Save current values as defaults (restored via restoredefaults()).
        self.default_lines = structfun(@(x) x.copy(), self.lines, ...
                                       'UniformOutput', false());
        savedefaults@mpcsim.Option(self);
    end%function
    
    function restoredefaults(self)
        % Restores defaults to the object.
        self.lines = structfun(@(x) x.copy(), self.default_lines, ...
                               'UniformOutput', false());
        restoredefaults@mpcsim.Option(self);
    end%function
end%methods

methods (Access=protected)
    function updateaxlims(self)
        % Updates the axis limits of the object.
        if isempty(self.ax)
            error('axes have not been initialized yet! Try self.initaxes().');
        end
        
        xlims = [-self.Npast, self.Nfuture]*self.Delta;
        
        ylims = [self.get('pltmin'), self.get('pltmax')];
        if any(isnan(ylims)) || any(isinf(ylims))
            set(self.ax, 'YLimMode', 'auto');
            ylims = get(self.ax, 'ylim');
        end
        
        set(self.ax, 'xlim', xlims, 'ylim', ylims);
        if isfield(self.lines, 'divider')
            self.lines.divider.y = ylims;
            self.lines.divider.update();
        end
    end%function
    
    function list = getdefaultlist(self, fields)
        % list = self.getdefaultlist(defaultfields)
        %
        % Returns a big list of keys by concatenating the fields from defaults.
        %
        % defaultfields can either be true() to include defaults, false() to
        % not include any, or a cell array of field names.
        %
        % Note that subclasses may define required fields that cannot be
        % removed.
        defaults = self.get_default_fields();
        if isequal(fields, true())
            fields = fieldnames(defaults);
        elseif isequal(fields, false())
            fields = {};
        elseif iscell(fields)
            fields = fields(:);
        else
            error('defaultfields must be a bool or cell array!');    
        end
        
        % Add in required fields.
        required = self.get_required_fields();
        defaults = mpctools.structupdate(required, defaults);
        fields = [fieldnames(required); fields];
        
        % Now generate list.
        items = cell(length(fields), 1);
        for i = 1:length(fields)
            f = fields{i};
            if ~ischar(f)
                error('field %d is not a string!', i);
            elseif ~isfield(defaults, f)
                error('field ''%s'' is not in defaults!');
            end
            items{i} = [{f}, defaults.(f)];
        end
        list = horzcat(items{:});    
    end%function
end%methods

methods (Static)
    function defaults = get_default_fields()
        % Returns a struct of default values for all of the object fields.
        % Intended to be overridden by subclasses.
        defaults = struct();
        defaults.noise = {0, '.name', 'Plant Noise', '.min', 0};
    end%function
    
    function required = get_required_fields()
        % Returns a struct of required values for all of the object fields.
        %
        % Removing these fields will lead to internal problems.
        required = struct();
        required.value = {0, '.visible', false()};
        required.min = {-inf(), '.max', 'max', '.name', 'Min Limit'};
        required.max = {inf(), '.min', 'min', '.name', 'Max Limit'};
        required.pltmin = {NaN(), '.name', 'Plot Min Limit'};
        required.pltmax = {NaN(), '.name', 'Plot Max Limit'};
        required.setpoint = {NaN(), '.name', 'Setpoint', '.visible', false()};
    end%function
end%function

end%classdef

