classdef SimCon < handle

properties (GetAccess = public, SetAccess = public)
    interval = 0.1; % Minimum timestep in seconds.
    data = struct(); % Struct for user data.
end%properties

properties (GetAccess = public, SetAccess = private)
    stepfunc;
    fig;
    axes;
    iteration = 0;
    MVs;
    XVs;
    CVs;
    DVs;
    Options;
    Nfuture;
    Npast;
    Delta = 1;
    isclosedloop = false();
    isrunning = false();
    buttons = struct();
    toolbar = [];
    plotvars = {};
    Nrows = 0;
    Ncols = 2;
    graphicssystem;
    menus = struct();
    history = struct();
end%properties

properties (GetAccess = public, SetAccess = public)
    isfirsttimeclosedloop = true();
    isfirsttimemhe = true();
end

methods (Access=public)
    function self = SimCon(varargin)
        % self = SimCon(mvs, cvs, options, [xvs] [dvs], [data],
        %               [Npast=100], [Nfuture=25])
        %
        % Initialize the object with menus, etc.
        %
        % Arguments can be passed positionally or as ('key', value) pairs.
        % Specific arguments are described below.
        %
        % * `stepfunc`
        %
        %   > Function handle to step function for simulation. Consult examples
        %   for function arguments and implementation.
        %
        % * `mvs`
        %
        %   > List of manipulated variables for the simulation. Should be a
        %   column cell of `MVObject` objects.
        %
        % * `cvs`
        %
        %   > List of controlled variables (i.e., measured outputs) for the
        %   simulation. Should be a column cell of `CVObject` objects.
        %
        % * `options`
        %
        %   > `Option` object defining custom options for the simualtion.
        %
        % * `xvs`
        %
        %   > List of state variables for the simulation. Should be a
        %   column cell of `XVObject` objects.
        %
        % * `dvs`
        %
        %   > List of disturbance variables for the simulation. Should be a
        %   column cell of `DVObject` objects.
        %
        % * `data`
        %
        %   > Struct of relevant data values for the simulation. Values can be
        %   used by the simulation step function.
        %
        % * `Npast`
        %
        %   > Number of past timesteps to show on plots.
        %
        % * `Nfuture`
        %
        %   > Number of future timesteps to show on plots.
        %
        % * `layout`
        %
        %   > 2D cell array defining layout for plots. Each entry should be a
        %   string of the form `[MCXD]V[0-9]+` giving the grid position of the
        %   plot for relevant MVs, CVs, XVx, and DVx.
        %
        %   > If not provided, the default layout is one column for each
        %   category of variable.
        %
        % * `history` : Struct
        %
        %   > Struct of matrices that store past history of certain values.
        %   Each of these matrices is used to initialize a mpcsim.VectorQueue
        %   object that can be modified by stepfunc.
        %
        %   > These `VectorQueue` objects are stored here (rather than in the
        %   data struct) so that they are properly reset if the simulation is
        %   reset.
        persistent parser
        if isempty(parser)
            parser = mpctools.ArgumentParser();
            parser.add('stepfunc', 'required', '<function_handle>');
            parser.add('mvs', 'required', 'cell');
            parser.add('cvs', 'required', 'cell');
            parser.add('options', 'required', '<mpcsim.Option>');
            parser.add('xvs', {}, 'cell');
            parser.add('dvs', {}, 'cell');
            parser.add('data', struct(), 'struct');
            parser.add('Npast', 100, {'scalar', 'pos', 'int'});
            parser.add('Nfuture', 25, {'scalar', 'pos', 'int'});
            parser.add('layout', {}, 'cell');
            parser.add('history', struct(), 'struct');
        end
        args = parser.parse(varargin{:});
        
        % Check graphics system.
        if mpctools.isOctave()
            self.graphicssystem = graphics_toolkit();
        else
            self.graphicssystem = 'matlab';
        end
        if isequal(self.graphicssystem, 'gnuplot')
            error('gnuplot is not a supported graphics system!');
        end
        
        % Save step function.
        self.stepfunc = args.stepfunc;
        
        % Create figure, clearing default menus.
        self.fig = figure('Toolbar', 'none');
        switch self.graphicssystem
        case 'fltk'
            mpcsim.clearmenus(self.fig);
        case 'matlab'
            set(self.fig, 'menubar', 'none');
        end
        
        % Add menus.
        self.menus.file = uimenu(self.fig, 'label', 'File');
        userinputs = uimenu(self.menus.file, 'label', 'User inputs via');
        uimenu(userinputs, 'label', 'dialog boxes', 'callback', ...
               mpcsim.func2callback(@() self.setterminalinput(false())));
        uimenu(userinputs, 'label', 'terminal', 'callback', ...
               mpcsim.func2callback(@() self.setterminalinput(true())));
        cbdebug = mpcsim.func2callback(@() self.debug());
        uimenu(self.menus.file, 'label', 'Debug', 'callback', cbdebug);
        cbreset = mpcsim.func2callback(@() self.reset());
        uimenu(self.menus.file, 'label', 'Reset', 'callback', cbreset);
        cbclose = mpcsim.func2callback(@() self.close());
        uimenu(self.menus.file, 'label', 'Quit', 'callback', cbclose);
        
        self.MVs = args.mvs(:);
        self.menus.MVs = uimenu(self.fig, 'label', 'MVs');
        for i = 1:length(self.MVs)
            self.MVs{i}.touimenu(self.menus.MVs);
        end
        
        self.XVs = args.xvs(:);
        if ~isempty(args.xvs)
            self.menus.XVs = uimenu(self.fig, 'label', 'XVs');
            for i = 1:length(self.XVs)
                self.XVs{i}.touimenu(self.menus.XVs);
            end
        end
        
        self.DVs = args.dvs(:);
        if ~isempty(args.dvs)
            self.menus.DVs = uimenu(self.fig, 'label', 'DVs');
            for i = 1:length(self.DVs)
                self.DVs{i}.touimenu(self.menus.DVs);
            end
        end
        
        self.CVs = args.cvs(:);
        self.menus.CVs = uimenu(self.fig, 'label', 'CVs');
        for i = 1:length(self.CVs)
            self.CVs{i}.touimenu(self.menus.CVs);
        end
        
        self.plotvars = [self.CVs; self.XVs; self.MVs; self.DVs];
        if isempty(args.layout)
            args.layout = self.getdefaultlayout();
        end
        
        self.Options = args.options;
        self.Options.savedefaults();
        self.menus.options = self.Options.touimenu(self.fig);
        
        if isequal(self.graphicssystem, 'qt')
            set(self.fig, 'menubar', 'none'); % Needed to remove default menus.
        end
        
        % Add buttons.
        if isequal(self.graphicssystem, 'fltk')
            % In FLTK, a callback can't be called a second time until the first
            % one finishes. This means we can't use a toggle for the run button.
            self.addbutton('play', 'Play', @() self.play_callback());
            self.addbutton('pause', 'Pause', @() self.pause_callback());
        else
            self.addbutton('run', 'Run', @(onoff) self.togglerun(onoff), true());
        end
        self.addbutton('step', 'Step', @() self.step_callback());
        self.addbutton('closedloop', 'Controller', ...
                       @(onoff) self.toggleclosedloop(onoff), true());
        self.addbutton('legend', 'Legend', ...
                        @(onoff) self.togglelegends(onoff), true());
        self.addbutton('resize', 'Resize Axes', @() self.resize_callback());
        
        % Choose timestep and save data.
        self.data = args.data; % Store user data.
        self.Delta = mpctools.structget(self.data, 'Delta', 1);
        
        % Add axes.
        self.Nfuture = args.Nfuture;
        self.Npast = args.Npast;
        self.initvars();
        self.initaxes(args.layout);
        self.resizeaxes();
        
        % Initialize history.
        self.history = struct();
        fields = fieldnames(args.history);
        for i = 1:length(fields)
            f = fields{i};
            self.history.(f) = mpcsim.VectorQueue(args.history.(f));
        end
        
        % Clean up.
        if mpctools.isOctave()
            more('off');
        end
        fprintf('SimCon initialization complete.\n');
    end%function
    
    function initvars(self)
        % Initialize variable objects and save default settings.
        for i = 1:length(self.plotvars)
            v = self.plotvars{i};
            v.Nfuture = self.Nfuture;
            v.Npast = self.Npast;
            v.Delta = self.Delta;
            v.savedefaults();
        end
    end%function
    
    function initaxes(self, layout)
        % Initializes the axes of the object.
        cols = {self.MVs, self.XVs, self.CVs};
        [self.Nrows, self.Ncols] = size(layout);
        self.axes = cell(size(layout));
        
        for j = 1:size(layout, 2)
            for i = 1:size(layout, 1)
                s = layout{i, j};
                if ~isempty(s)
                    index = str2double(s(3:end));
                    switch lower(s(1:2))
                    case 'mv'
                        col = self.MVs;
                    case 'cv'
                        col = self.CVs;
                    case 'xv'
                        col = self.XVs;
                    case 'dv'
                        col = self.DVs;
                    otherwise
                        col = NaN();
                    end
                    if isequal(col, NaN()) || isnan(index)
                        error('Invalid layout string: %s!', s);
                    end
                    
                    v = col{index};
                    self.axes{i, j} = self.subplotij(i, j);
                    v.initaxes(self.axes{i, j});
                    v.savedefaults(); % Update with lines.
                end
            end
            xlabel('Time');
        end
    end%function
    
    function resizeaxes(self)
        % Resizes the axes to have a tighter layout.
        bbox = [0, 0, 1, 1];
        mpcsim.tight_layout(self.fig, self.axes, bbox);
    end%function
    
    function mainloop(self)
        % Runs until the figure window is closed.
        uiwait(self.fig);
    end%function    
    
    function runloop(self)
        % Starts a loop of calling step.
        self.buttons.step.disable();
        while self.isrunning
            start = cputime();
            self.step();
            pause(self.interval - (cputime() - start));
        end
        self.buttons.step.enable();
    end%function
    
    function step(self)
        % Take one step and update plot.
        self.stepfunc(self, self.iteration, self.checkchflag());
        self.updatelines();
        drawnow();
        self.iteration = self.iteration + 1;
    end%function
    
    function updatelines(self)
        % Updates all the lines with the current data.
        for i = 1:length(self.plotvars)
            v = self.plotvars{i};
            if v.is_plotted()
                v.updatelines();
            end
        end
    end%function
    
    function play_callback(self)
        % Play button callback for FLTK.
        self.isrunning = true();
        self.buttons.play.disable();
        self.runloop();
    end%function
    
    function pause_callback(self)
        % Pause button callback for FLTK.
        self.isrunning = false();
        self.buttons.play.enable();
    end%function
    
    function step_callback(self)
        self.step();
    end%function
    
    function resize_callback(self)
        self.resizeaxes();
    end%function
    
    function debug(self)
        % Start debugger for object.
        fprintf('Starting debugger for %s object.\n', class(self));
        keyboard();
    end%function
    
    function reset(self)
        % Resets the object to its default settings.
        fields = fieldnames(self.history);
        for i = 1:length(fields)
            self.history.(fields{i}).reset();
        end
        toreset = [self.MVs(:); self.CVs(:); {self.Options}];
        for i = 1:length(toreset)
            toreset{i}.restoredefaults();
        end
        self.updatelines();
    end%function
    
    function togglerun(self, onoff)
        self.isrunning = onoff;
        if onoff
            self.runloop();
        end
    end%function
    
    function togglelegends(self, onoff)
        % Toggles the legends for all of the plots.
        for i = 1:length(self.plotvars)
            self.plotvars{i}.togglelegend(onoff);
        end
    end%function
    
    function toggleclosedloop(self, onoff)
        % Toggles whether the system is in closed-loop or not.
        self.isclosedloop = onoff;
    end%function
    
    function addbutton(self, name, label, callback, toggle)
        % self.addbutton(name, label, callback, [toggle=False])
        %
        % Add a button to the GUI.
        %
        % If toggle is false, callback should be a handle that takes no
        % arguments. It is called whenever the button is pressed, and the button
        % is automatically reset.
        %
        % If toggle is true, callback should be a handle that takes one boolean
        % argument that states whether the item is being enabled (True) or
        % disabled (False).
        narginchk(4, inf());
        if nargin() < 5
            toggle = false();
        end
        
        % Add button.
        if ismember(self.graphicssystem, {'fltk', 'qt'})
            if ~isfield(self.menus, 'separator')
                self.menus.separator = uimenu(self.fig, 'label', ' | ', ...
                                              'foregroundcolor', [0, 0, 0], ...
                                              'selectionhighlight', 'off');
            end
            self.buttons.(name) = mpcsim.Button(self.fig, label, callback, toggle);
            if isequal(self.graphicssystem, 'fltk')
                self.buttons.(name).copyto(self.menus.separator);
            end
        else
            if isempty(self.toolbar)
                self.toolbar = uitoolbar(self.fig);
            end
            parent = self.toolbar;
            self.buttons.(name) = mpcsim.Button(parent, label, callback, ...
                                                toggle, mpcsim.geticon(name));
        end
    end%function
    
    function v = getvec(self, list, field)
        % v = self.getvec(list, field)
        %
        % Gets the given field from each element in list and returns a column
        % vector. list can be a cell array (e.g., self.CVs, self.MVs, or 
        % self.XVs), or it can be a string 'CVs', 'MVs', or 'XVs'.
        %
        % Aways returns a column vector of length numel(field).
        narginchk(3, 3);
        if ischar(list) || (iscell(list) && ischar(list{1}))
            list = self.getvarlist(list);
        end
        v = cellfun(@(i) i.get(field), list(:));
    end%function
    
    function setvec(self, list, field, val)
        % self.setvec(list, field, val)
        %
        % Sets the given field from each element in list and returns a column
        % vector. list can be a cell array (e.g., self.CVs, self.MVs, or 
        % self.XVs), or it can be a string 'CVs', 'MVs', or 'XVs'.
        narginchk(4, 4);
        if ischar(list) || (iscell(list) && ischar(list{1}))
            list = self.getvarlist(list);
        end
        if numel(list) ~= numel(val)
            error('Expected %d elements, got %d!', numel(list), numel(val));
        end
        for i = 1:numel(list)
            list{i}.set(field, val(i));
        end
    end%function
    
    function d = getplotdata(self, list, linename)
        % d = self.getplotdata(list, linename)
        %
        % Returns an array of the current plot data for the given variables
        % and line. list should be a string 'y', 'x', 'u', or 'd', and linename
        % should be the name of a plot line, e.g., 'past', 'future', etc.
        %
        % Values are all NaNs for variables that aren't plotted. If no values
        % are plotted, returns a single sample of NaNs.
        narginchk(3, 3);
        d = [];
        list = self.getvarlist(list);
        for i = 1:length(list)
            var = list{i};
            if var.is_plotted()
                if ~isfield(var.lines, linename)
                    error('Unknown line name "%s"!', linename);
                end
                thisline = var.lines.(linename);
                if isempty(d)
                    d = NaN(length(list), length(thisline.y));
                end
                d(i,:) = thisline.y;
            end
        end
        if isempty(d)
            d = NaN(length(list), 1);
        end
    end%function
    
    function setpred(self, var, vals, t)
        % self.setpred(var, vals, [t])
        %
        % Set future predictions for the object.
        %
        % var can be 'u', 'x', 'y', 'yconstu', or 'xmhe'. vals should be a
        % matrix of values with time in the second dimension.
        %
        % Third argument t can be 'future', 'past', or a vector of time points.
        narginchk(3, 4);
        if nargin() < 4
            t = 'future';
        end
        
        % Decide which field.
        list = self.getvarlist(var);
        switch var
        case 'yconstu'
            linename = 'constu';
        case {'xmhe', 'dmhe'}
            linename = 'mhe';
        otherwise
            linename = 'future';
        end
        if isequal(var, 'u')
            extra = 0;
        else
            extra = -1;
        end
        
        % Check size.
        if size(vals, 1) ~= length(list)
            error('vals has %d rows but expected %d!', ...
                  size(vals, 1), length(list));
        end
        
        % Calculate time.
        Nt = size(vals, 2) + extra;
        switch t
        case 'future'
            t = (0:Nt)*self.Delta;
        case 'past'
            t = (-Nt:0)*self.Delta;
        otherwise
            t = t(:);
            if ~isnumeric(t) || length(t) ~= Nt + 1
                error('Given t is invalid!');
            end
        end
        
        % Now actually update values.
        for i = 1:length(list)
            var = list{i};
            if var.is_plotted()
                line = var.lines.(linename);
                line.x = t;
                line.y = vals(i,:);
            end
        end
    end%function
    
    function setterminalinput(self, tf)
        % Sets whether to use a terminal (true) or dialog boxes (false) for
        % input from users.
        opts = [self.MVs(:); self.CVs(:); self.XVs(:); ...
                self.DVs(:); {self.Options}];
        for i = 1:length(opts)
            opts{i}.terminal = tf;
        end
    end%function
    
    function setdisturbancemodel(self, model)
        % self.setdisturbancemodel(model)
        %
        % Sets the current disturbance model using boolean vectors. Note that
        % the only effect is to disable the disturbance model weights for the
        % unselected disturbances.
        %
        % model should be a 
        narginchk(2, 2);
        
        % Disable everything and then enable the active ones.
        lists = {[self.CVs; self.MVs; self.DVs], self.getvarlist(model)};
        enables = {'off', 'on'};
        for i = 1:length(lists)
            for j = 1:length(lists{i})
                var = lists{i}{j};
                if isfield(var.submenus, 'dnoise')
                    set(var.submenus.dnoise, 'enable', enables{i});
                end
            end
        end
    end%function
    
    function close(self)
        % Closes the simcon.
        close(self.fig);
    end%function
end%methods

methods (Access=private)
    function chflag = checkchflag(self, resetflag)
        % chflag = self.checkchflag([reset=True])
        %
        % checks whether chflag is set on any CV, MV, XV, or Option.
        % Also returns true on the first iteration.
        %
        % If reset is True, each flag is reset to False once it is read.
        narginchk(1, 2);
        if nargin() < 2
            resetflag = true();
        end    
        tocheck = [self.MVs(:); self.XVs(:); self.CVs(:); {self.Options}];
        chflag = (nargout() == 0) || (self.iteration == 0);
            % Can skip checks if no output is requested.
        
        for i = 1:length(tocheck)
            chflag = chflag || tocheck{i}.chflag;
            if resetflag
                tocheck{i}.chflag = false();
            elseif chflag
                break
            end    
        end
    end%methods
    
    function ax = subplotij(self, i, j)
        % Returns a handle to the (i, j) subplot.
        k = self.Ncols*(i - 1) + j;
        figure(self.fig);
        ax = subplot(self.Nrows, self.Ncols, k);
    end%function
    
    function list = getvarlist(self, var)
        % Returns the list associated with string var. Can be 'x', 'u', or 'y'.
        %
        % Can also be a cell vector of strings of the form [var][index]
        % where [var] is either 'y', 'u', or 'd', and [index] is the integer
        % index.
        narginchk(2, 2);
        if ischar(var)
            switch lower(var)
            case {'u', 'mvs'}
                list = self.MVs;
            case {'x', 'xvs', 'xmhe'}
                list = self.XVs;
            case {'y', 'cvs', 'yconstu'}
                list = self.CVs;
            case {'d', 'dvs', 'dmhe'}
                list = self.DVs;
            otherwise
                error('Invalid variable: "%s"!', var);
            end
        elseif iscell(var)
            list = cell(length(var), 1);
            menus = struct('y', {self.CVs}, 'u', {self.MVs}, 'd', {self.DVs});
            for i = 1:length(var)
                k = var{i}(1);
                j = str2double(var{i}(2:end));
                list{i} = menus.(k){j};
            end
        else
            error('Invalid input for var!');
        end
    end%function
    
    function layout = getdefaultlayout(self)
        % Returns default layout for the object.
        cols = {self.MVs, self.XVs, self.CVs, self.DVs};
        names = {'mv', 'xv', 'cv', 'dv'};
        nmax = max(cellfun(@length, cols));
        layout = cell(nmax, 4);
        keep = true(1, 4);
        for j = 1:4
            keep(j) = ~isempty(cols{j});
            n = length(cols{j});
            col = arrayfun(@(i) sprintf('%s%d', names{j}, i), (1:n)', ...
                                    'UniformOutput', false());
            col = [col; repmat({''}, nmax - n, 1)];
            layout(:,j) = col;
        end
        layout = layout(:,keep);
    end%function
end%methods

end%classdef
