classdef DVObject < mpcsim.PlotVar

methods
    function self = DVObject(varargin)
        % self = DVObject(name, [label], [defaults],
        %                 'field1', val1, 'field2', val2, ...)
        %
        % Initializes the object.
        %
        % Defaults can be True to set defaults, False to not include any, or
        % it can be a cell array of strings to only use certain defaults.
        self = self@mpcsim.PlotVar(varargin{:});
    end%function
    
    function initaxes(self, ax_)
        % Sets up the plot for the current object.
        narginchk(2, 2);
        initaxes@mpcsim.PlotVar(self, ax_);
        self.lines.setpoint.label = ''; % Don't show in legend.
        self.addline('estpast', 'Estimated', ...
                     (-self.Npast:0)*self.Delta, ...
                     self.get('estvalue')*ones(self.Npast + 1, 1), '--b');
        self.addline('mhe', 'Current MHE', NaN(), NaN(), '--c');
    end%function
    
    function updatelines(self, dostep)
        % Updates the current plots.
        narginchk(1, 2);
        if nargin() < 2
            dostep = true();
        end
        if dostep
            self.lines.future.y = self.get('estvalue')*ones(self.Nfuture + 1, 1);
            estpast = self.lines.estpast;
            estpast.y = [estpast.y(2:(self.Npast + 1)); self.get('estvalue')];
        end
        updatelines@mpcsim.PlotVar(self, dostep);
    end%function
end%methods

methods (Static)
    function defaults = get_default_fields()
        % Returns a struct of default values for all of the object fields.
        % Note that this is the the class defaults, and not the values used for
        % initialization.
        defaults = mpcsim.PlotVar.get_default_fields();
        defaults.value = {0, '.visible', true()};
        defaults.dnoise = {1, '.min', 0, '.name', 'Disturbance Model Noise (Qed)'};
    end%function
    
    function required = get_required_fields()
        required = mpcsim.PlotVar.get_required_fields();
        required.estvalue = {NaN(), '.name', 'Est. Value', '.visible', false()};
    end%function
end%methods

end%classdef
