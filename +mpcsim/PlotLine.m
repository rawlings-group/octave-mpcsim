classdef PlotLine < handle
% PlotLine
%
% Simple container to hold a line for an MPCSim plot.

properties
    line;
    x;
    y;
    stairstep;
    label = '';
end%properties

methods
    function self = PlotLine(ax, x_, y_, style, stairstep_, label_, line_)
        % self = PlotLine(ax, x, y, style, [stairstep], [label], [line])
        %
        % Creates the object from x, y data and adds to plot.
        %
        % Style can be either a linestyle string (e.g., '-ok'), or a cell array
        % of ('property', value) pairs that are passed to plot().
        %
        % If line_ is passed, it is used as the plot handle, and style is
        % ignored.
        narginchk(4, 7);
        if nargin() < 5
            stairstep_ = false();
        end
        if nargin() < 6
            label_ = '';
        end
        if nargin() < 7
            line_ = [];
        end
        self.x = x_(:);
        self.y = y_(:);
        if isempty(line_)
            if ~iscell(style)
                style = {style};
            end
            line_ = plot(ax, NaN(), NaN(), style{:});
        end
        self.line = line_;
        self.stairstep = stairstep_;
        self.label = label_;
        self.update();
    end%function
    
    function update(self)
        % Updates the current line.
        if isempty(self.line)
            error('line has been deleted! Cannot update!');
        end
        x_ = self.x(:);
        y_ = self.y(:);
        if self.stairstep
            if length(x_) == length(y_) + 1
                y_ = [y_; y_(end)];
            end
            [x_, y_] = stairs(x_, y_);
        end
        set(self.line, 'xdata', x_, 'ydata', y_);
    end%function
    
    function delete(self)
        % Deletes the line.
        self.x = zeros(0, 1);
        self.y = zeros(0, 1);
        delete(self.line);
        self.line = [];
    end%function
    
    function c = copy(self)
        % Makes a copy of the object using the same line handle.
        c = mpcsim.PlotLine([], self.x, self.y, [], self.stairstep, ...
                            self.label, self.line);
    end%function
end%methods

end%classdef

