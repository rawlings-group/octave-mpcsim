classdef Option < handle
% Option
%
% Handle class for holding information about MPCSim variables and objects.

properties (Access=public)
    name;
    chflag = false();
    terminal = false();
    rootmenu = [];
    submenus = struct();
end

properties (Access=protected)
    indices = struct();
    data;
    default_data;
    Nf;
    strsubfields = {'type'}; % List of subfields that are string-valued.
end

methods (Access=public)
    function self = Option(name_, varargin)
        % self = Option(name, 'field1', val, 'field2', val, ...)
        %
        % Creates a Option from ('field', value) pairs. Each field should be
        % the name of a field, optionally suffixed with a dot and a subfield
        % from the following list:
        %
        % - value: scalar value for field (used if no subfield is specified)
        % - min, max: limits that value can take
        % - name: displayed name for field as a string
        % - chflag: whether changing the value of the object sets the changed
        %           flag for the whole object. Default False.
        % - visible: whether the value can be set via ask(). Default True.
        %
        % These subfields can be set using 'field.subfield' as an argument. Note
        % that the 'field' part can be omitted (but not the dot) if the field
        % has just been given. E.g.,
        %
        %    Option('a', 0, '.min', -1, '.max', 1)
        %
        % is shorthand for
        %
        %    Option('a.value', 0, 'a.min', -1, 'a.max', 1)
        %
        % Note that if there are duplicates, only the final value is used.
        if nargin() < 1
            error('name is required!');
        elseif ~ischar(name_)
            error('name must be a string!');
        end
        self.name = name_;
        self.init(varargin{:});
    end%function
    
    function init(self, varargin)
        % self.init('field1', val, 'field2', val, ...)
        %
        % Reinitialize values. See Option constructor for more details.
        if mod(length(varargin), 2) ~= 0
            error('Arguments must come in pairs!');
        end
        
        % Get unique list of fields and initialize.
        parts = cell(length(varargin)/2, 2);
        prev = '';
        for i = 1:size(parts, 1)
            parts(i,:) = mpcsim.Option.dotsplit(varargin{2*i - 1}, 2);
            if isempty(parts{i,1})
                parts{i,1} = prev;
            end
            prev = parts{i,1};
            if isempty(parts{i,2})
                parts{i,2} = 'value';
            end
        end
        vals = varargin(2:2:end);
        [~, ifields] = unique(parts(:,1), 'first');
        fields = parts(sort(ifields),1);
        self.Nf = length(fields);
        
        % Validate field names.
        for i = 1:self.Nf
            f = fields{i};
            if ~ischar(f)
                error('Field %d is not a string!', i);
            elseif ~isvarname(f) && ~iskeyword(f)
                error('Field ''%s'' is invalid! Must be a variable name.', f);
            end
        end
        
        % Get default display names.
        displaynames = cell(size(fields));
        for i = 1:self.Nf
            f = fields{i};
            words = regexp(f, '\<[a-z]');
            f(words) = upper(f(words));
            displaynames{i} = f; 
        end
        
        % Get mapping.
        self.indices = struct();
        for i = 1:self.Nf
            self.indices.(fields{i}) = i;
        end
        
        % Fill in values, mins, maxes, etc.
        self.data = struct('field', fields, ...
                           'value', num2cell(zeros(self.Nf, 1)), ...
                           'min', cell(self.Nf, 1), 'max', cell(self.Nf, 1), ...
                           'name', displaynames, ...
                           'chflag', num2cell(false(self.Nf, 1)), ...
                           'type', repmat({'float'}, self.Nf, 1), ...
                           'visible', num2cell(true(self.Nf, 1)));
        for i = 1:size(parts, 1)
            [field, subfield] = self.parsefield(parts{i,:});
            self.set_(field, subfield, vals{i});
        end
    end%function
    
    function ask(self, field)
        % self.ask(field)
        %
        % Opens a dialog box to ask for a given value.
        narginchk(2, 2);
        [field, subfield, i] = self.parsefield(field);
        displayname = self.data(i).name;
        if ~isequal(subfield, 'value')
            error('Can only set ''value'' subfield, not ''%s''!', subfield);
        elseif ~self.data(i).visible
            error('Field ''%s'' cannot be set!', field);
        end    
        
        % Ask for value.
        kwargs = struct();
        kwargs.prompt = sprintf('Set %s:', displayname);
        kwargs.current = self.get_(field, 'value');
        kwargs.min = self.get_(field, 'min');
        kwargs.max = self.get_(field, 'max');
        kwargs.type = self.get_(field, 'type');
        kwargs.title = self.name;
        kwargs.terminal = self.terminal;
        
        % Ask value and update if something was given.
        val = mpcsim.askval('**', kwargs);
        if ~isempty(val)
            self.set_(field, subfield, val);
        end
    end%function
    
    function val = get(self, field, default)
        % val = self.get(field, [default])
        %
        % Returns the data from the given field.
        %
        % Optional argument default gives the value to return if not found.
        % Otherwise, an error is raised if not found.
        narginchk(2, 3);
        if ~ischar(field)
            error('field must be a string!');
        end
        [field, subfield] = self.parsefield(field);
        if isfield(self.indices, field)
            val = self.get_(field, subfield);
        elseif nargin() >= 3
            val = default;
        else
            error('Field ''%s'' is not found!', field);
        end
    end%function  
    
    function val = nanget(self, field)
        % val = self.nanget(field)
        %
        % Shortcut for self.get(field, NaN), returning NaN as the default
        % value if field is not found.
        val = self.get(field, NaN());
    end%function
    
    function set(self, field, val)
        % self.set(field, val)
        %
        % Sets value of given field. Subfields can be set using
        % 'field.subfield'.
        narginchk(3, 3);
        [field, subfield] = self.parsefield(field);
        self.set_(field, subfield, val);
    end%function
    
    function [root, sub] = touimenu(self, parent, store)
        % [rootmenu, submenus] = self.touimenu(parent, [store=True])
        %
        % Converts the object to a uimenu attached to the given parent.
        %
        % If store is true, then the menus are stored to self.rootmenu and
        % self.submenus of the current object.
        %
        % Returned value menus is a struct with fields 
        narginchk(2, 3);
        if nargin() < 3
            store = true();
        end
        
        % Create root menu and add nodes.
        root = uimenu(parent, 'label', self.name);
        sub = struct();
        for i = 1:self.Nf
            if self.data(i).visible
                field = self.data(i).field;
                callback = mpcsim.func2callback(@() self.ask(field));
                sub.(field) = uimenu(root, 'label', self.data(i).name, ...
                                     'callback', callback);
           end
        end
        
        % Store to object.
        if store
            self.rootmenu = root;
            self.submenus = sub;
        end
    end%function
    
    function s = disp(self)
        % Return string representation of object.
        header = sprintf('%s("%s")\n{\n', class(self), self.name);
        fields = fieldnames(self.data);
        parts = cell(2 + length(fields), self.Nf);
        for i = 1:self.Nf
            thisdata = self.data(i);
            parts{1,i} = sprintf('    "%s" : {\n', thisdata.field);
            for p = 1:length(fields)
                f = fields{p};
                val = thisdata.(f);
                if isempty(val) || isequal(f, 'field')
                    parts{p + 1, i} = '';
                else
                    if ischar(val)
                        fmt = '"%s"';
                    elseif isscalar(val) && (isnumeric(val) || islogical(val))
                        fmt = '%g';
                    else
                        error('val must be a string or numeric scalar!');
                    end
                    parts{p + 1, i} = sprintf(['        "%s" : ', fmt, ...
                                               ',\n'], f, val);
                end
            end    
            parts{end,i} = sprintf('    }\n');
        end
        footer = sprintf('}\n');
        s = [header, parts{:}, footer];
        if nargout() == 0
            disp(s);
        end
    end%function
    
    function savedefaults(self)
        % Save current values as defaults (restored via restoredefaults()).
        self.default_data = self.data;
    end%function
    
    function restoredefaults(self)
        % Restores defaults to the object.
        self.data = self.default_data;
        self.chflag = true();
    end%function
end%methods

methods (Access=private)
    function [field, subfield, index] = parsefield(self, field, subfield)
        % [field, subfield, index] = self.parsefield(field, [subfield])
        %
        % Parses a string of the form 'field' or 'field.subfield', returning
        % the index and subfield.
        %
        % Note that subfield can be given explicitly, at which point this
        % function only checks that both are valid.
        narginchk(2, 3)
        if nargin() < 3
            parts = self.dotsplit(field);
            switch length(parts)
            case 0
                error('No fields found in string!');
            case 1
                parts = [parts, {'value'}];
            case 2
                % Pass.
            otherwise
                error('Too many fields in ''%s''! At most 2 allowed!', field);
            end
            [field, subfield] = deal(parts{:});
        end
        
        if ~isfield(self.data, subfield)
            error('Subfield ''%s'' is invalid!', subfield);
        end
        
        if nargout() > 2
            index = self.indices.(field);
        end
    end%function
    
    function set_(self, field, subfield, val)
        % Low-level set command that requires field and subfield.
        %
        % Note that this function does not do any checking, so you should be
        % sure to validate field and subfield first.
        %
        % If the field's chflag is set, also sets the global chflag.
        narginchk(4, 4);
        if isequal(subfield, 'field')
            error('field names cannot be changed!');
        end
        index = self.indices.(field);
        self.data(index).(subfield) = val;
        if self.data(index).chflag
            self.chflag = true();
        end
    end%function
    
    function val = get_(self, field, subfield)
        % Low-level getting function without error checking.
        narginchk(3, 3);
        
        index = self.indices.(field);
        val = self.data(index).(subfield);
        
        tries = 1;
        while ischar(val) && ~ismember(subfield, self.strsubfields) && tries < self.Nf
            [~, subfield, index] = self.parsefield(val);
            val = self.data(index).(subfield);
            tries = tries + 1;
        end
        if tries >= self.Nf
            error('Too many redirects!');
        end
    end%function
end%methods

methods (Static)
    function parts = dotsplit(str, N)
        % Returns a cell array of the elements of string str split at periods.
        %
        % Second argument N states how many parts to return (padding with empty
        % strings as necessary).
        narginchk(1, 2);
        if ~ischar(str)
            error('Input must be a character string!');
        end
        str = str(:)';
        dots = (str == '.');
        segments = diff([0, find(dots), length(str) + 1]) - 1;
        parts = mat2cell(str(~dots), 1, segments);
        if nargin() >= 2
            parts = [parts, repmat({''}, 1, N - length(parts))];
            parts = parts(1:end);
        end
    end%function
end%methods

end%classdef
