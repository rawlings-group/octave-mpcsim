function callback = func2callback(func)
% callback = func2callback(func)
%
% Wraps the function func (which must take no arguments) to take
% the default arguments h (handle) and evt (event).
callback = @(h, evt) func();
end%function