function cstr_mpcsim()
% Example control of a nonlinear cstr.
% See Pannocchia and Rawlings, AIChE J, 2002.
% Options are provided for various disturbance model options
% and for linear or nonlinear MPC.
  
% Load the control package

if mpctools.isOctave()
    pkg('load', 'control');
end

% Initialize random seed

randn('state', 927);

% Define system sizes.

Nx   = 3;            % number of states
Nu   = 2;            % number of inputs
Ny   = 3;            % number of outputs
Nid  = 3;            % number of integrating disturbances
Nd   = 1;            % number of unmeasured disturbances.
Nw   = Nx + Nid;     % number of augmented states
Nv   = Ny;           % number of output measurements
Nt   = 60;           % controller prediction horizon
Nmhe = 60;           % MHE Estimation horizon.
Ns   = 2*Ny;         % number of slack variables for soft output constraints

% Define sample time in min.

Delta = 1;

% Define plot windows

Npast = 120;   % Length of past to plot
Nfuture = 60;  % Lenght of future to plot

% Define the measurement matrix

C = eye(Ny, Nx);     % all states are measured.

% Define parameters for cstr

T0 = 350; % K
c0 =  1;  % kmol/m^3
r = 0.219; % m
k0 = 7.2e10; % min^-1
E =  8750; % K
U =  54.94; % kJ/(min m^2 K)
rho = 1e3;   % kg/m^3
Cp = 0.239;  % kJ/(kg K)
DeltaH = -5e4; % kJ/kmol
A = pi()*r.^2;
rhoCp = rho*Cp;

% Define hard state bounds for range of model validity. The desired bounds
% xlb and xub are included elsewhere as soft constraints.

xmax = [2.0; 1000.0; 1.0];
xmin = [0.0;    0.0; 0.0];

% Package up parameters.

pars = mpcsim.captureworkspace();
pars.ode = @(x, u, d) ode_pars(x, u, d, pars);

% Define initial conditions for x, u, y, and d.

pars.x_init = [0.878; 324.5; 0.659];
pars.u_init = [300.0;   0.1];
pars.y_init = C*pars.x_init;
pars.d_init = [0.1];
pars.dmodel = {}; 

% Set MHE data for y and u.

pars.y    = repmat(pars.y_init, 1, Nmhe + 1);
pars.u    = repmat(pars.u_init, 1, Nmhe);
pars.dhat = zeros(Nid, 1);

% Save a history of xhat for use with the prior.

histstruct = struct('xhat', repmat(pars.x_init, 1, Npast), ...
                    'dhat', repmat(zeros(Nid, 1), 1, Npast));

% Create variables. First arguments are (short name, description, defaults)

MV1 = mpcsim.MVObject('Tc', 'mv - coolant temperature (K)', true(), ...
    'pltmin', 290.0, 'pltmax', 310.0, 'min', 255.0, 'max', 345.0, ...
    'ssrvalue', 1.0e-6, 'svalue', 0.1, 'rvalue', 0.001, ...
    'roclim', 100.0, 'value', pars.u_init(1));

MV2 = mpcsim.MVObject('F', 'mv - outlet flowrate (m3/min)', true(), ...
    'pltmin', 0.08, 'pltmax', 0.12, 'min', 0.01, 'max', 0.29, ...
    'ssrvalue', 1.0e-6, 'svalue', 100.0, 'rvalue', 0.001, ...
    'roclim', 0.3, 'value', pars.u_init(2), 'dnoise', 1.0);

cvslack = {'lbslack', 1000, '.min', 0, '.name', 'LB Viol. Penalty', ...
           'ubslack', 1000, '.min', 0, '.name', 'UB Viol. Penalty'};

cvnosp = {'setpoint', NaN(), '.visible', false(), 'ssqvalue', NaN()};

CV1 = mpcsim.CVObject('c', 'cv - concentration A (kmol/m3)', true(), ...
    'pltmin', 0.8, 'pltmax', 0.95, 'min', 0.05, 'max', 1.35, ...
    'qvalue', 1.0, 'mnoise', 2.7759e-2, 'dnoise', 3.1623e-3, 'value',pars.y_init(1), ...
    'estvalue', pars.y_init(1), 'setpoint', pars.y_init(1), ...
    'ssqvalue', 10.0, cvslack{:}, 'noise', 0.0);

CV2 = mpcsim.CVObject('T', 'cv - temperature (K)', true(), ...
    'pltmin', 315.0, 'pltmax', 335.0, 'min', 0.0, 'max', 1000.0, ...
    'qvalue', 0.0, 'mnoise', 1.02616, 'dnoise',  3.1623e-3, 'value', pars.y_init(2), ...
    'estvalue', pars.y_init(2), ...
     'ssqvalue', 0.0, cvnosp{:}, cvslack{:}, 'noise', 0.0);

CV3 = mpcsim.CVObject('h', 'cv - level (m)', true(), ...
    'pltmin', 0.6, 'pltmax', 0.8, 'min', 0.05, 'max', 0.95, ...
    'qvalue', 1.0, 'mnoise', 2.0839e-2, 'dnoise', 0.1, 'value', pars.y_init(3), ...
    'estvalue', pars.y_init(3), 'setpoint', pars.y_init(3), ...
    'ssqvalue', 1.0, cvslack{:}, 'noise', 0.0);

%    Raug = diag([7.7058e-6, 1.053, 4.3428e-6]);    
    
XV1 = mpcsim.XVObject('c', 'xv - concentration A (kmol/m3)', true(), ...
    'pltmin', 0.8, 'pltmax', 0.95, 'dnoise',  3.1623e-3,  ...
    'value', pars.x_init(1), 'estvalue', pars.x_init(1), 'noise', 0.0);

XV2 = mpcsim.XVObject('T', 'xv - temperature (K)', true(), ...
    'pltmin', 315.0, 'pltmax', 335.0, 'dnoise',  3.1623e-3, ...
    'value', pars.x_init(2), 'estvalue', pars.x_init(2), 'noise', 0.0);

XV3 = mpcsim.XVObject('h', 'xv - level (m)', true(), ...
    'pltmin', 0.6, 'pltmax', 0.8, 'dnoise',  3.1623e-3, ...
    'value', pars.x_init(3), 'estvalue', pars.x_init(3), 'noise', 0.0);

DV1 = mpcsim.DVObject('FO', 'dv - inlet flowrate (m3/min)', true(), ...
    'pltmin', 0.08, 'pltmax', 0.12, 'value', pars.d_init(1), ...
    'dnoise', 0.1, 'noise', 0.0);
    
options = mpcsim.Option('Options', ...
    'noisefactor', 0, '.name', 'Noise Factor', ...
    'linmpc', true(), '.name', 'Linear MPC', '.type', 'bool', ...
    'statefeedback', false(), '.name', 'State Feedback', '.type', 'bool', ...
    'priorweight', 1, '.name', 'MHE Prior Weight (Pe)', '.min', 0, ...
    'dmodnum', 3, '.name', 'Disturbance Model Number', '.type', 'int', 'min', 1, 'max', 3);

% Create gui.

layout = {'MV1', 'XV1', 'CV1'; 'MV2', 'XV2', 'CV2'; 'DV1', 'XV3', 'CV3'};
simcon = mpcsim.SimCon(@simstep, {MV1, MV2}, {CV1, CV2, CV3}, options, ...
                       'xvs', {XV1, XV2, XV3}, 'dvs', {DV1}, 'data', pars, ...
                       'Npast', Npast, 'Nfuture', Nfuture, 'layout', layout, ...
                       'history', histstruct);

simcon.step(); % Step once to initialize everything.
simcon.mainloop();

end%function

% Define ODE function for the cstr

function dxdt = ode_pars(x, u, d, pars)

    % Nonlinear ODE model for cstr

    % Unpack state, input, and disturbance

    c  = x(1);
    T  = x(2);
    h  = x(3) + eps(); % Avoids division by zero.
    Tc = u(1);
    F  = u(2);
    F0 = d(1);

    % Calculate reaction rate   

    k    = pars.k0*exp(-pars.E/T);
    rate = k*c;
    
    % Calculate derivatives

    dcdt = F0*(pars.c0 - c)/(pars.A*h) - rate;
    dTdt = F0*(pars.T0 - T)/(pars.A*h) ...
           - pars.DeltaH/pars.rhoCp*rate ...
           + 2*pars.U/(pars.r*pars.rhoCp)*(Tc - T); 
    dhdt = (F0 - F)/pars.A;
    dxdt = [dcdt; dTdt; dhdt];

end%function

% Define simulation function

function xsim = rk4sim(ode, Delta, x, u)

    % Simulates an ODE with disturbance model using RK4.

    Nsim = size(u, 2);
    xsim = NaN(length(x), Nsim + 1);
    xsim(:,1) = x;
    for i = 1:Nsim
        f = @(x) ode(x, u(:,i));
        xsim(:,i + 1) = mpctools.rk4_(Delta, f, xsim(:,i));
    end

end%function

% Define steady-state cost function

function cost = targetcost(y, ysp, Q, u, usp, R, s, p)

    % Steady-state target cost. s is a vector of state constraint slacks.

    dy = y - ysp;
    du = u - usp;
    cost = dy'*Q*dy + du'*R*du + p'*s;

end%function

% Define dynamic stage cost function

function cost = stagecost(x, xsp, Q, u, usp, R, Du, S, s, p)

    % Stage cost. S is rate-of-change penalty for u. s is vector of state
    % constraint slacks. p is penalty vector for output constraints.

    cost = targetcost(x, xsp, Q, u, usp, R, s, p) + Du'*S*Du;

end%function

% Define soft output constraint slack function

function con = slackoutputs(y, s, ylb, yub)

    % Slacked output constraints.

    Ny = length(y);
    slb = s(1:Ny);
    sub = s((Ny + 1):end);
    con = [
        y - yub - sub;
        ylb - y - slb;
    ];

end%function

% Define simulation functions

function simstep(simcon, iteration, stale)

    % Runs one step of the cstr example

    fprintf('Iteration %4d -----------------------------------\n', iteration);
    mpc = import_mpctools();
    
    % Unpack stuff from simulation container.

    parnames = {'xscale', 'yscale', 'uscale', 'dscale', 'C', 'Nx', 'Ny', ...
                'Nu', 'Nd', 'Nid', 'Ns', 'Nw', 'Nv', 'Nt', 'Nmhe', 'ode', ...
                'xmin', 'xmax', 'Delta', 'y', 'u', 'x_init', 'y_init', ...
                'u_init', 'sim', 'controller', 'mhe', 'sstarg', 'linmodel', ...
                'd_init', 'dhat'};
    [xscale, yscale, uscale, dscale, C, Nx, Ny, Nu, Nd, Nid, Ns, Nw, Nv, ...
        Nt, Nmhe, ode, xmin, xmax, Delta, y, u, x_init, y_init, u_init, ...
        sim, controller, mhe, sstarg, linmodel, d_init, dhat] ...
        = mpctools.structdeal({simcon.data, []}, parnames{:});

    % Get option flag values

    nonlinmpc     = ~simcon.Options.get('linmpc');
    statefeedback = simcon.Options.get('statefeedback');
    dmodnum       = simcon.Options.get('dmodnum');

    % Construct new disturbance model is necessary

    if (iteration == 0)
        olddmodnum = 0;
    else
        olddmodnum = simcon.data.dmodnum;
    end  
    
    if ~isequal(dmodnum, olddmodnum)

        switch dmodnum

        case 1 % Output disturbances on c, h (Not enough integrating disgurbances)

            Nid = 2;
            Bd = zeros(Nx, Nid);
            Cd = [1 0 ; 0 0; 0 1];
            dmodel = {'y1','y3'};
            dhat = zeros(Nid,1);
            stale = true();
            
        case 2 % Output disturbances on c, T, h (Not detectable)
    
            Nid = 3;
            Bd = zeros(Nx, Nid);
            Cd = [1 0 0; 0 1 0; 0 0 1];
            dmodel = {'y1','y2','y3'};
            dhat = zeros(Nid,1);
            stale = true();
    
    
        case 3 % output disturbances on c, h, input disturbance on F (Offset free)
    
            Nid = 3;
            Bd = zeros(Nx, Nid);
            Bd(:,3) = [0.1655; 97.91; -6.637];
            Cd = [1 0 0; 0 0 0; 0 1 0];
            dmodel = {'y1','y3','u2'};
            dhat = zeros(Nid,1);
            stale = true();
    
        end


        simcon.setdisturbancemodel(dmodel);
        fprintf('  Choosing disturbance model [%s]\n', ...
                mpctools.row2str(dmodel)');
        simcon.data.dmodel = dmodel;
        simcon.data.dmodnum = dmodnum;
        simcon.data.Bd = Bd;
        simcon.data.Cd = Cd;
        simcon.data.Nid = Nid;
        simcon.data.Nw = Nx + Nid;
        Bd
        Cd
        
    end
    
    % Grab bounds and penalty coefficients

    ulb = simcon.getvec('MVs', 'min');
    uub = simcon.getvec('MVs', 'max');
    dulim = simcon.getvec('MVs', 'roclim');
    
    xsys = simcon.getvec('XVs', 'value');
    usys = simcon.getvec('MVs', 'value');
    mpcpar = struct();
    mpcpar.Q = diag(simcon.getvec('CVs', 'qvalue'));
    mpcpar.P = mpcpar.Q;
    mpcpar.R = diag(simcon.getvec('MVs', 'rvalue'));
    mpcpar.S = diag(simcon.getvec('MVs', 'svalue'));
    mpcpar.p = [ ...
        simcon.getvec('CVs', 'lbslack');
        simcon.getvec('CVs', 'ubslack');
    ];
    mpcpar.xsp = zeros(Nx, 1); % Gets set later.
    mpcpar.usp = zeros(Nu, 1); % Gets set later.
    mpcpar.uprev = usys;
    mpcpar.ylb = simcon.getvec('CVs', 'min');
    mpcpar.yub = simcon.getvec('CVs', 'max');
    
    mpcpar.C = C;
    mpcpar.Bd = simcon.data.Bd;
    mpcpar.Cd = simcon.data.Cd;
    mpcpar.dhat = zeros(Nid, 1);
    
    sstargpar = rmfield(mpcpar, {'P', 'S', 'uprev'});
    sstargpar.Q = diag([simcon.CVs{1}.get('ssqvalue'); 0.0; simcon.CVs{3}.get('ssqvalue')]);
    sstargpar.R = diag(simcon.getvec('MVs', 'ssrvalue'));
    sstargpar.ysp = zeros(Ny, 1); % Gets set later.
    
    mhepar = struct();
    mhepar.Qinv = diag(1./[simcon.getvec('XVs', 'dnoise').^2; ...
                           simcon.getvec(simcon.data.dmodel, 'dnoise').^2]);
    mhepar.Rinv = diag(1./(simcon.getvec('CVs', 'mnoise').^2));
    mhepar.Pinv = mhepar.Qinv; % Will be set later.
    mhepar.xbar = [simcon.history.xhat.get(-Nmhe);
                   simcon.history.dhat.get(-Nmhe)];
    lxbar = length(mhepar.xbar);
    Nw = simcon.data.Nw;
    if (lxbar > Nw)
        mhepar.xbar = mhepar.xbar((1:Nw),:);
    end
    if (lxbar < Nw)
        ldiff = Nw - lxbar;
        xfill = zeros(ldiff,1);
        mhepar.xbar((lxbar+1):Nw,:) = xfill;
    end   
    mhepar.C = C;
    mhepar.Bd = simcon.data.Bd;
    mhepar.Cd = simcon.data.Cd;
        
    % Get disturbance model
        
    Bd = simcon.data.Bd;
    Cd = simcon.data.Cd;
    Nid = simcon.data.Nid;
    Nw = simcon.data.Nw;

    % Rebuild items if necessary

    if stale
        fprintf('  Building controllers ');
        
        
        % Make a one-step simulator and a multi-step simulator

        simargs = {};
        sim = mpc.getCasadiIntegrator(ode, Delta, [Nx, Nu, Nd], ...
                                      {'x', 'u', 'd'}, 'funcname', 'sim');
        simcon.data.sim = sim;
        fprintf('.');
        
        % Get functions for the controller

        ftarg = mpc.getCasadiFunc(@(x, u, Bd, dhat) ode(x, u, d_init) + Bd*dhat, ...
                                  {Nx, Nu, [Nx, Nid], Nid}, ...
                                  {'x', 'u', 'Bd', 'dhat'}, ...
                                 'funcname', 'f');
        fmpc = mpc.getCasadiFunc(@(x, u, Bd, dhat) ode(x, u, d_init) + Bd*dhat, ...
                                 {Nx, Nu, [Nx, Nid], Nid}, ...
                                 {'x', 'u', 'Bd', 'dhat'}, ...
                                 'rk4', true(), 'Delta', Delta, ...
                                 'funcname', 'f');
        fmhe = mpc.getCasadiFunc(@(x, u, Bd) [ode(x(1:Nx), u, d_init) ...
                                              + Bd*x((Nx + 1):end);
                                              zeros(Nid, 1)], ...
                                 {Nx + Nid, Nu, [Nx, Nid]}, ...
                                 {'x', 'u', 'Bd'}, ...
                                 'rk4', true(), 'Delta', Delta, ...
                                 'funcname', 'faug');
        
        htarg = mpc.getCasadiFunc(@(C, x, Cd, dhat) C*x + Cd*dhat, ...
                                  {[Ny, Nx], Nx, [Ny, Nid], Nid}, ...
                                  {'C', 'x', 'Cd', 'dhat'}, ...
                                  'funcname', 'h');
        hmpc = htarg;
        hmhe = mpc.getCasadiFunc(@(C, x, Cd) C*x(1:Nx) + Cd*(x((Nx + 1):end)), ...
                                 {[Ny, Nx], Nx + Nid, [Ny, Nid]}, ...
                                 {'C', 'x', 'Cd'}, 'funcname', 'haug');
        
        e = mpc.getCasadiFunc(@slackoutputs, [Ny, Ns, Ny, Ny], ...
                              {'y', 's', 'ylb', 'yub'}, 'funcname', 'e');
        
        ltarg = mpc.getCasadiFunc(@targetcost, ...
            {Ny, Ny,   [Ny,Ny], Nu, Nu, [Nu,Nu],Ns, Ns}, ...
            {'y','ysp','Q',    'u','usp','R',  's','p'}, ...
            'funcname', 'ltarg');
        lmpc = mpc.getCasadiFunc(@stagecost, ...
            {Nx, Nx,   [Nx,Nx], Nu, Nu, [Nu,Nu], Nu,[Nu,Nu], Ns, Ns}, ...
            {'x','xsp','Q',    'u','usp','R',   'Du','S',    's','p'}, ...
            'funcname', 'l');
        lmhe = mpc.getCasadiFunc(@(w, Qinv, v, Rinv) w'*Qinv*w + v'*Rinv*v, ...
                                 {Nw, [Nw, Nw], Nv, [Nv, Nv]}, ...
                                 {'w', 'Qinv', 'v', 'Rinv'}, 'funcname', 'lmhe');
        
        Vf = mpc.getCasadiFunc(@(x, xsp, P) (x - xsp)'*P*(x - xsp), ...
                               {Nx, Nx, [Nx, Nx]}, {'x','xsp','P'}, ...
                               'funcname', 'Vf');
        
        lx = mpc.getCasadiFunc(@(x, xbar, Pinv) (x - xbar)'*Pinv*(x - xbar), ...
                               {Nx + Nid, Nx + Nid, (Nx + Nid)*[1, 1]}, ...
                               {'x', 'xbar', 'Pinv'}, 'funcname', 'lx');
        fprintf('.');
        
        % Assemble controller

        N = struct('x', Nx, 'u', Nu, 'y', Ny, 's', Ns, 't', Nt);
        lb = struct('u', ulb, 'x', xmin);
        ub = struct('u', uub, 'x', xmax);
        guess = struct('u', repmat(mpcpar.usp, 1, Nt));
        controller = mpc.nmpc('f', fmpc, 'e', e, 'h', hmpc, 'l', lmpc, ...
                              'Vf', Vf, 'N', N, ...
                              'lb', lb, 'ub', ub, 'guess', guess, ...
                              'par', mpcpar, 'verbosity', 0);
        simcon.data.controller = controller;
        fprintf('.');
        
        % Assemble steady-state target finder

        N = struct('x', Nx, 'u', Nu, 'y', Ny, 's', Ns);
        lb = struct('u', ulb, 'x', xmin);
        ub = struct('u', uub, 'x', xmax);
        guess = struct('u', mpcpar.usp);
        sstarg = mpc.sstarg('f', ftarg, 'e', e, 'h', htarg, 'l', ltarg, ...
                            'N', N, 'lb', lb, 'ub', ub, 'guess', guess, ...
                            'par', sstargpar, 'discretef', false(), ...
                            'verbosity', 0);
        simcon.data.sstarg = sstarg;
        fprintf('.');
        
        % Assemble moving horizon estimator - disturbance model is output bias

        N = struct('x', Nx + Nid, 'y', Ny, 'u', Nu, 't', Nmhe);
        lb = struct('x', [xmin; -inf(Nid, 1)]);
        ub = struct('x', [xmax; inf(Nid, 1)]);
        mhe = mpc.nmhe('f', fmhe, 'h', hmhe, 'l', lmhe, 'lx', lx, ...
                       'y', y, 'u', u, 'N', N, 'lb', lb, 'ub', ub, ...
                       'par', mhepar, 'verbosity', 0, 'wadditive', true());
        simcon.data.mhe = mhe;
        fprintf('.');
        
        % Get linear model

        linmodel = struct('xlin', x_init, 'ulin', u_init, 'ylin', y_init);
        [linmodel.A, linmodel.B, linmodel.Bd] = mpc.getLinearizedModel(sim, ...
            {simcon.data.x_init, simcon.data.u_init, simcon.data.d_init}, ...
            {'A', 'B', 'Bd'}, 'deal', true());
        linmodel.C = C;
        simcon.data.linmodel = linmodel;    
       
        % All done
        
        fprintf('done\n');
        
    end
        
    % Advance actual system with noise

    noise = simcon.Options.get('noisefactor');
    usys = usys + noise*randn(Nu, 1).*simcon.getvec('u', 'noise');
    dsys = simcon.getvec('DVs', 'value') ...
           + noise*randn(Nd, 1).*simcon.getvec('d', 'noise');
    if iteration > 0
        xsys = full(sim(xsys, usys, dsys));
        xsys = xsys + noise*randn(Nx, 1).*simcon.getvec('x', 'noise');
    end
    ysys = C*xsys;
    ysys = ysys + noise*randn(Ny, 1).*simcon.getvec('y', 'noise');
    simcon.data.y = [simcon.data.y(:,2:end), ysys];
    simcon.data.u = [simcon.data.u(:,2:end), usys];
    
    % Construct augmented linear system and compute Kalman Filter gain

    Aaug = [linmodel.A, mhepar.Bd; zeros(Nid, Nx), eye(Nid)];
    Gaug = eye(Nx + Nid);
    Baug = [linmodel.B; zeros(Nid, Nu)];
    Caug = [linmodel.C, mhepar.Cd];
    Qaug = diag(1./diag(mhepar.Qinv));
    Raug = diag(1./diag(mhepar.Rinv));
    % Raug(isinf(Raug)) = 1e12; % dlqe doesn't like infinite values here.
    [Laug, Paug] = dlqe(Aaug, Gaug, Caug, Qaug, Raug);

    % Test disturbance model for detectability
        
    Naug = size(Aaug,1);
    detec = rank([eye(Naug) - Aaug; Caug]);
    if detec < Nx + Nid
        fprintf(' * Augmented system is not detectable!\n')
    end
    
    % Test for not enough integrating disturbances
    
    if Nid < Ny
        fprintf(' * Not enough integrating disturbances!\n')
    end
    
    if statefeedback

        % Just take actual state.

        xhat = xsys;
        dhat = zeros(Nid, 1);
        xmhe = NaN(Nx, Nmhe + 1);
        dmhe = NaN(Nid, Nmhe + 1);

    elseif nonlinmpc

        % Run MHE

        mhepar.Pinv = simcon.Options.get('priorweight')*mpctools.spdinv(Paug);
        mhepar.y = simcon.data.y;
        mhepar.u = simcon.data.u;
        mhe.par = mhepar;
        mhe.lb.x = repmat([xmin; -inf(Nid, 1)], 1, Nmhe + 1);
        mhe.ub.x = repmat([xmax; inf(Nid, 1)], 1, Nmhe + 1);
        if (stale | simcon.isfirsttimemhe)
            mhe.guess.x = repmat([xsys; zeros(Nid, 1)], 1, Nmhe + 1);
        end
        mhe.solve();
        fprintf('  Estimator: %s (Obj: %g)\n', mhe.status, mhe.obj);
        mhe.saveguess();
        
        xhat = mhe.var.x(1:Nx,end);
        dhat = mhe.var.x((Nx + 1):end,end);
        xmhe = mhe.var.x(1:Nx,:);
        dmhe = mhe.var.x((Nx + 1):end,:);

        if simcon.isfirsttimemhe
          simcon.isfirsttimemhe = false();
        end
        
    else

        if ~simcon.isfirsttimemhe
          simcon.isfirsttimemhe = true();
        end
    
        % Use Kalman filter.

        xhat = [simcon.getvec('x', 'estvalue'); dhat];
        xlin = [linmodel.xlin; zeros(Nid, 1)];
        ulin = linmodel.ulin;
        ylin = linmodel.ylin;
        xhatm = Aaug*(xhat - xlin) + Baug*(usys - ulin) + xlin;
        yhatm = Caug*(xhatm - xlin) + ylin;
        xhat = xhatm + Laug*(ysys - yhatm);
        
        dhat = xhat((Nx + 1):end);
        xhat = xhat(1:Nx);
        xmhe = NaN(Nx, Nmhe + 1);
        dmhe = NaN(Nid, Nmhe + 1);

    end

    simcon.history.xhat.pushpop(xhat);
    dhata = dhat;
    if (Nid == 2)
        dhata = [dhat; 0.0];
    end
    simcon.history.dhat.pushpop(dhata); 
    simcon.data.dhat = dhat;
    yhat = C*xhat + simcon.data.Cd*dhat;
    
    % Update plots
    
    simcon.setvec('x', 'value', xsys);
    simcon.setvec('y', 'value', ysys);
    simcon.setvec('x', 'estvalue', xhat);
    simcon.setvec('y', 'estvalue', yhat);
    if isequal(simcon.data.dmodel{2}, 'd1')
        simcon.setvec('d', 'estvalue', dhat(2));
    end
    
    % Run controller and simulate predictions
    
    multisim = @(x, u) rk4sim(@(x, u) ode(x, u, d_init) + simcon.data.Bd*dhat, ...
                                          Delta, x, u);
    if simcon.isclosedloop
        
%        fprintf('  isfirstimeclosedloop = %4d \n', simcon.isfirsttimeclosedloop);
%        fprintf('  stale                = %4d \n', stale);

        % Steady-state target finder
        
        if (stale | simcon.isfirsttimeclosedloop)
            sstarg.guess.x = xsys;
            sstarg.guess.u = usys;
        end
        csp = simcon.CVs{1}.get('setpoint');
        hsp = simcon.CVs{3}.get('setpoint');
        
        if nonlinmpc
            % Use nonlinear target finder.
            sstarg.par = sstargpar;
            sstarg.par.ylb(1) = csp;
            sstarg.par.yub(1) = csp;
            sstarg.par.ysp(1) = csp;
            sstarg.par.ylb(3) = hsp;
            sstarg.par.yub(3) = hsp;
            sstarg.par.ysp(3) = hsp;
            sstarg.par.dhat = dhat;

            sstarg.lb.u = ulb;
            sstarg.ub.u = uub;
            sstarg.solve();
            fprintf('  Target Finder: %s (Obj: %g)\n', sstarg.status, sstarg.obj);
            sstarg.saveguess();
            
            xtarg = sstarg.var.x;
            utarg = sstarg.var.u;
        else
            % Use linear target finder.
            H = [1, 0, 0; 0, 0, 1];
            Ginv = [eye(Nx) - linmodel.A, -linmodel.B; ...
                    H*linmodel.C, zeros(size(H,1), Nu)]\eye(Nx + Nu);
            ysp = [csp; 0; hsp] - linmodel.ylin;
            targ = Ginv*[mhepar.Bd*dhat; H*(ysp - mhepar.Cd*dhat)];
            xtarg = targ(1:Nx) + linmodel.xlin;
            utarg = targ((Nx + 1):end) + linmodel.ulin;
        end
        
        % Controller
        
        if (stale | simcon.isfirsttimeclosedloop)
            controller.guess.x = repmat(xsys, 1, Nt + 1);
        end
        controller.par = mpcpar;
        controller.par.xsp = xtarg;
        controller.par.usp = utarg;
        controller.par.dhat = dhat;
        controller.lb.u = repmat(ulb, 1, Nt);
        controller.ub.u = repmat(uub, 1, Nt);
        controller.lb.Du = repmat(-dulim, 1, Nt);
        controller.ub.Du = repmat(dulim, 1, Nt);
        if nonlinmpc
            controller.fixvar('x', 1, xhat);
            controller.solve();
            fprintf('  Controller: %s (Obj: %g)\n', controller.status, ...
                    controller.obj);
            controller.saveguess();
            usys = controller.var.u(:,1);
            usim = controller.var.u;
            xsim = controller.var.x;
        else
            Aaug = blkdiag(linmodel.A, zeros(Nu, Nu));
            Baug = [linmodel.B; eye(Nu)];
            Qaug = blkdiag(mpcpar.Q, mpcpar.S);
            Raug = mpcpar.R + mpcpar.S;
            Maug = [zeros(Nx, Nu); -mpcpar.S];
            Kaug = -dlqr(Aaug, Baug, Qaug, Raug, Maug);
            Kx = Kaug(:,1:Nx);
            Ku = Kaug(:,(Nx + 1):end);
            usim = NaN(Nu, Nt);
            xsim = NaN(Nx, Nt + 1);
            xsim(:,1) = xhat;
            uprev = mpcpar.uprev;
            for i = 1:Nt
                usim(:,i) = Kx*(xsim(:,i) - xtarg) + Ku*(uprev - utarg) + utarg;
                usim(:,i) = min(max(usim(:,i), ulb), uub);
                uprev = usim(:,i);
                xsim(:,i + 1) = linmodel.A*(xsim(:,i) - xtarg)...
                                + linmodel.B*(usim(:,i) - utarg) ...
                                + xtarg;
            end
            usys = usim(:,1);  
        end
        
        % Reset flag for first time closed loop
        
        if simcon.isfirsttimeclosedloop
            simcon.isfirsttimeclosedloop = false();
        end
    else
        
        % Set flag for first time closed loop
        
        if ~simcon.isfirsttimeclosedloop
            simcon.isfirsttimeclosedloop = true();
        end

        % Make forecast assuming constant input.
        
        usim = repmat(usys, 1, Nt);
        xsim = multisim(xhat, usim);
        
        xtarg = NaN(Nx, 1);
        utarg = NaN(Nu, 1);
    end

    simcon.setvec('u', 'value', usys);
    ysim = bsxfun(@plus, C*xsim, simcon.data.Cd*dhat);
    
    % Simulate constant-u system
       
    uconst = repmat(usys, 1, Nt);
    xconst = multisim(xhat, uconst);
    yconst = bsxfun(@plus, C*xconst, simcon.data.Cd*dhat);
    
    % Update forecasts
       
    simcon.setvec('x', 'setpoint', xtarg);
    simcon.setvec('u', 'setpoint', utarg);
    simcon.setpred('x', xsim);
    simcon.setpred('y', ysim);
    simcon.setpred('u', usim);
    simcon.setpred('yconstu', yconst);
    simcon.setpred('xmhe', xmhe, 'past');
    if isequal(simcon.data.dmodel{2}, 'd1')
        simcon.setpred('dmhe', dmhe(2,:), 'past');
    end
    
    % Done
    fprintf('--------------------------------------------------\n');
    
end%function

