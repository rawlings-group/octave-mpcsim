function hotairballoon()
% Example control of a hot air balloon.
if mpctools.isOctave()
    pkg('load', 'control');
end
randn('state', 927);

% Define system sizes.
Nx   = 3;            % number of states
Nu   = 2;            % number of inputs
Ny   = 3;            % number of outputs
Nid  = 3;            % number of integrating disturbances
C = eye(Ny, Nx); % All states are measured.

Nd = 1; % Number of unmeasured disturbances.

Nw   = Nx + Nid;     % number of augmented states
Nv   = Ny;           % number of output measurements

Ns = 2*Ny; % Number of slack variables for soft output constraints.

Nt = 60; % Controller prediction horizon.
Nmhe = 60; % MHE Estimation horizon.

Delta = 10; % Sample time in s.

Npast = 120;
Nfuture = 60;

% Define scaling factors
hr = 1000  ; % altitude scale factor in m
tr = 10.10 ; % time scale factor in s
vr = hr/tr ; % velocity scaling vactor in m/s
Tr = 288.2 ; % temperature scale factor in K
fr = 4870  ; % fuel valve postion scale factor in %
pr = 1485  ; % vent valve position scale factor in %

tscale = tr;
xscale = [hr; vr; Tr];
yscale = xscale; % x and y are both in engineering units.
uscale = [fr; pr];
dscale = [vr]; %#ok<*NBRAK> % Disturbance is an updraft.

% Define parameters for hot-air balloon
alpha  = 5.098;   % balloon number
gamma  = 5.257;   % atmosphere number
mu     = 0.1961;  % ratio payload weight to total weight
omega  = 8.544;   % drag coefficient
delta  = 0.0255;  % temperature drop-off coefficient
beta   = 0.01683; % heat transfer coefficient

% Define hard state bounds for range of model validity. The desired bounds
% xlb and xub are included elsewhere as soft constraints.
xmin = [-10; -50; 0];
xmax = [10000; 50; 200];

% Package up parameters.
pars = mpcsim.captureworkspace(); %#ok<*NASGU>
pars.ode = @(x, u, d) ode_pars(x, u, d, pars);
pars.fuelerror = 0; % Quantization error for fuel.

% Define initial conditions for x, u, y, and d.
pars.x_init = [0; 0; 85.37];
pars.u_init = [20; 0];
pars.y_init = C*pars.x_init;
pars.d_init = [0];
pars.dmodel = {}; % Gets set later.

% Set MHE data for y and u.
pars.y = repmat(pars.y_init, 1, Nmhe + 1);
pars.u = repmat(pars.u_init, 1, Nmhe);
pars.dhat = zeros(Nid, 1);

% Save a history of xhat for use with the prior.
histstruct = struct('xhat', repmat(pars.x_init, 1, Npast), ...
                    'dhat', repmat(zeros(Nid, 1), 1, Npast));

% Create variables. First arguments are (short name, description, defaults)
MV1 = mpcsim.MVObject('f', 'fuel flow (%)', true(), ...
    'pltmin', -5.0, 'pltmax', 105.0, 'min', 0.0, 'max', 100.0, ...
    'ssrvalue', 0.001, 'svalue', 5.0, 'rvalue', 0.1, ...
    'roclim', 20.0, 'value', pars.u_init(1));

MV2 = mpcsim.MVObject('p', 'top vent position (%)', true(), ...
    'pltmin', -10.0, 'pltmax', 110.0, 'min', 0, ...
    'max', 100, 'ssrvalue', 10.0, 'svalue', 0.1, 'rvalue', 10.0, ...
    'roclim', 20.0, 'value', pars.u_init(2));

cvslack = {'lbslack', 1000, '.min', 0, '.name', 'LB Viol. Penalty', ...
           'ubslack', 1000, '.min', 0, '.name', 'UB Viol. Penalty'};

cvnosp = {'setpoint', NaN(), '.visible', false(), 'ssqvalue', NaN()};

CV1 = mpcsim.CVObject('h', 'altitude (m)', true(), ...
    'pltmin', -300.0, 'pltmax', 7300.0, 'min', 0.0, ...
    'max', 7000.0, 'qvalue', 1, 'mnoise', 0.1, ...
    'dnoise', 0.1, 'value', pars.y_init(1), 'estvalue', pars.y_init(1), ...
    'setpoint', 0.0, 'ssqvalue', 1, cvslack{:}, 'noise', 0.0);

CV2 = mpcsim.CVObject('v', 'vertical velocity (m/s)', true(), ...
    'pltmin', -25.0, 'pltmax', 25.0, 'min', -23.0, 'max', 23.0, ...
    'qvalue', 0.0, 'mnoise', 0.32, 'dnoise', 0.1, 'value', pars.y_init(2), ...
    'estvalue', pars.y_init(2), cvnosp{:}, cvslack{:}, 'noise', 0.0);

CV3 = mpcsim.CVObject('T', 'bag temperature (*C)', true(), ...
    'pltmin', 30.0, 'pltmax', 150.0, 'min', 60.0, 'max', 120.0, ...
    'qvalue', 0.0, 'mnoise', 0.1, 'dnoise', 0.1, 'value', pars.y_init(3), ...
    'estvalue', pars.y_init(3), cvnosp{:}, cvslack{:}, 'noise', 0.025);

XV1 = mpcsim.XVObject('h', 'altitude (m)', true(), ...
    'pltmin', -300, 'pltmax', 7300, 'mnoise', 0.1,  ...
    'value', pars.x_init(1), 'estvalue', pars.x_init(1), 'noise', 0.0);

XV2 = mpcsim.XVObject('v', 'vertical velocity (m/s)', true(), ... 
    'pltmin', -25, 'pltmax', 25, 'mnoise', 0.1, ...
    'value', pars.x_init(2), 'estvalue', pars.x_init(2), 'noise', 0.0);

XV3 = mpcsim.XVObject('T', 'bag temperature (*C)', true(), ... 
    'pltmin', 30, 'pltmax', 150, 'mnoise', 0.1, ...
    'value', pars.x_init(3), 'estvalue', pars.x_init(3), 'noise', 0.0);

DV1 = mpcsim.DVObject('v_d', 'updraft (m/s)', true(), ...
    'pltmin', -25, 'pltmax', 25, 'value', pars.d_init(1), ...
    'dnoise', 0.32, 'noise', 0.0);
    
options = mpcsim.Option('Options', ...
    'noisefactor', 0, '.name', 'Noise Factor', ...
    'fuel', 0, '.name', 'Fuel Increment', ...
    'linmpc', false(), '.name', 'Linear MPC', '.type', 'bool', ...
    'statefeedback', false(), '.name', 'State Feedback', '.type', 'bool', ...
    'priorweight', 1, '.name', 'MHE Prior Weight (Pe)', '.min', 0, ...
    'altdmodel', false(), '.name', 'Alt. Disturbance Model', '.type', 'bool');

% Create gui.
layout = {'MV1', 'XV1', 'CV1'; 'MV2', 'XV2', 'CV2'; 'DV1', 'XV3', 'CV3'};
simcon = mpcsim.SimCon(@simstep, {MV1, MV2}, {CV1, CV2, CV3}, options, ...
                       'xvs', {XV1, XV2, XV3}, 'dvs', {DV1}, 'data', pars, ...
                       'Npast', Npast, 'Nfuture', Nfuture, 'layout', layout, ...
                       'history', histstruct);

simcon.step(); % Step once to initialize everything.
simcon.mainloop();

end%function

% ODE and measurement functions for the hot-air balloon.
function dxdt = ode_pars(x, u, d, pars)
    % ODE for hot air balloon system.
    x = (x + [0; 0; 273.15])./pars.xscale;
    u = u./pars.uscale;
    d = d./pars.dscale;
    lift = pars.alpha*pars.mu*(1 - pars.delta*x(1))^(pars.gamma - 1) ...
           *(1 - (1 - pars.delta*x(1))/x(3));
    drag = pars.omega*x(2)*abs(x(2));
    cooling = (x(3) - 1 + pars.delta*x(1))*(pars.beta + u(2));
    dxdt = [ ...
        x(2) + d(1);
        lift - pars.mu - drag;
        -cooling + u(1);
    ].*pars.xscale/pars.tscale;
end%function

function xsim = rk4sim(ode, Delta, x, u)
    % Simulates an ODE with disturbance model using RK4.
    Nsim = size(u, 2);
    xsim = NaN(length(x), Nsim + 1);
    xsim(:,1) = x;
    for i = 1:Nsim
        f = @(x) ode(x, u(:,i));
        xsim(:,i + 1) = mpctools.rk4_(Delta, f, xsim(:,i));
    end
end%function

function cost = targetcost(y, ysp, Q, u, usp, R, s, p)
    % Steady-state target cost. s is a vector of state constraint slacks.
    dy = y - ysp;
    du = u - usp;
    cost = dy'*Q*dy + du'*R*du + p'*s;
end%function

function cost = stagecost(x, xsp, Q, u, usp, R, Du, S, s, p)
    % Stage cost. S is rate-of-change penalty for u. s is vector of state
    % constraint slacks. p is penalty vector for output constraints.
    cost = targetcost(x, xsp, Q, u, usp, R, s, p) + Du'*S*Du;
end%function

function con = slackoutputs(y, s, ylb, yub)
    % Slacked output constraints.
    Ny = length(y);
    slb = s(1:Ny);
    sub = s((Ny + 1):end);
    con = [
        y - yub - sub;
        ylb - y - slb;
    ];
end%function

function x = checkground(x)
    % Adjusts x so that it does not go below the ground.
    x = full(x); % Make sure we don't have a Casadi DMatrix.
    jbad = (x(1,:) < 0);
    x(1,jbad) = 0;
    x(2,jbad) = max(x(2,jbad), 0);
end%function

function [u, err] = quantizefuel(u, ulb, uub, increment, err)
    % Quantizes the fuel usage to have the given increment.
    minfuel = ulb(1);
    maxfuel = uub(1);
            
    % Use cumulative rounding strategy.
    quantum = (maxfuel - minfuel)*increment;
    wantfuel = u(1,:)/quantum;
    Nmax = floor(maxfuel/quantum);
    Nmin = ceil(minfuel/quantum);
    wantsofar = 0;
    getsofar = err/quantum;
    for i = 1:size(u, 2)
        uwant = u(1,i)/quantum;
        wantsofar = wantsofar + uwant;
        uget = min(max(round(wantsofar - getsofar), Nmin), Nmax);
        if i == 1
            err = (uget - uwant)*quantum;
        end    
        u(1,i) = uget*quantum;
        getsofar = getsofar + uget;
    end
end%function

% Define simulation functions.
function simstep(simcon, iteration, stale)
    % Runs one step of the hot air balloon example.
    fprintf('Iteration %4d -----------------------------------\n', iteration);
    mpc = import_mpctools();
    
    % Unpack stuff from simulation container.
    parnames = {'xscale', 'yscale', 'uscale', 'dscale', 'C', 'Nx', 'Ny', ...
                'Nu', 'Nd', 'Nid', 'Ns', 'Nw', 'Nv', 'Nt', 'Nmhe', 'ode', ...
                'xmin', 'xmax', 'Delta', 'y', 'u', 'x_init', 'y_init', ...
                'u_init', 'sim', 'controller', 'mhe', 'sstarg', 'linmodel'...
                'd_init', 'dhat'};
    [xscale, yscale, uscale, dscale, C, Nx, Ny, Nu, Nd, Nid, Ns, Nw, Nv, ...
        Nt, Nmhe, ode, xmin, xmax, Delta, y, u, x_init, y_init, u_init, ...
        sim, controller, mhe, sstarg, linmodel, d_init, dhat] ...
            = mpctools.structdeal({simcon.data, []}, parnames{:});
    olddmodel = simcon.data.dmodel;
    
    fuelincrement = simcon.Options.get('fuel');
    nonlinmpc = ~simcon.Options.get('linmpc');
    statefeedback = simcon.Options.get('statefeedback');
    if simcon.Options.get('altdmodel')
        Bd = [0, 1, 0; 0, 0, 0; 0, 0, 0];
        Cd = [1, 0, 0; 0, 0, 0; 0, 0, 1];
        dmodel = {'y1'; 'd1'; 'y3'};
        idscale = [yscale(1); dscale(1); yscale(3)];
    else
        Bd = zeros(Nx, Nid);
        Cd = eye(Ny, Nid);
        dmodel = {'y1'; 'y2'; 'y3'};
        idscale = yscale;
    end
    if ~isequal(dmodel, olddmodel)
        simcon.setdisturbancemodel(dmodel);
        fprintf('  Choosing disturbance model [%s].\n', ...
                mpctools.row2str(dmodel'));
    end
    simcon.data.dmodel = dmodel;
    
    % Grab bounds and penalty coefficients.
    ulb = simcon.getvec('MVs', 'min');
    uub = simcon.getvec('MVs', 'max');
    dulim = simcon.getvec('MVs', 'roclim');
    
    xsys = simcon.getvec('XVs', 'value');
    usys = simcon.getvec('MVs', 'value');
    mpcpar = struct();
    mpcpar.Q = diag(simcon.getvec('CVs', 'qvalue')./xscale.^2);
    mpcpar.P = mpcpar.Q;
    mpcpar.R = diag(simcon.getvec('MVs', 'rvalue')./uscale.^2);
    mpcpar.S = diag(simcon.getvec('MVs', 'svalue')./uscale.^2);
    mpcpar.p = [ ...
        simcon.getvec('CVs', 'lbslack')./xscale;
        simcon.getvec('CVs', 'ubslack')./xscale;
    ];
    mpcpar.xsp = zeros(Nx, 1); % Gets set later.
    mpcpar.usp = zeros(Nu, 1); % Gets set later.
    mpcpar.uprev = usys;
    mpcpar.ylb = simcon.getvec('CVs', 'min');
    mpcpar.yub = simcon.getvec('CVs', 'max');
    
    mpcpar.C = C;
    mpcpar.Bd = Bd;
    mpcpar.Cd = Cd;
    mpcpar.dhat = zeros(Nid, 1);
    
    sstargpar = rmfield(mpcpar, {'P', 'S', 'uprev'});
    sstargpar.Q = diag([simcon.CVs{1}.get('ssqvalue'); 0; 0]);
    sstargpar.R = diag(simcon.getvec('MVs', 'ssrvalue'));
    sstargpar.ysp = zeros(Ny, 1); % Gets set later.
    
    mhepar = struct();
    mhepar.Qinv = diag(1./[simcon.getvec('XVs', 'mnoise').^2.*xscale.^2; ...
                           simcon.getvec(dmodel, 'dnoise').^2.*idscale.^2]);
    mhepar.Rinv = diag(1./(simcon.getvec('CVs', 'mnoise').^2.*yscale.^2));
    mhepar.Pinv = mhepar.Qinv; % Will be set later.
    mhepar.xbar = [simcon.history.xhat.get(-Nmhe);
                   simcon.history.dhat.get(-Nmhe)];
    mhepar.C = C;
    mhepar.Bd = Bd;
    mhepar.Cd = Cd;
    
    % Rebuild items if necessary.
    if stale
        fprintf('  Building controllers');
        
        % Make a one-step simulator and a multi-step simulator.
        simargs = {};
        sim = mpc.getCasadiIntegrator(ode, Delta, [Nx, Nu, Nd], ...
                                      {'x', 'u', 'd'}, 'funcname', 'sim');
        simcon.data.sim = sim;
        fprintf('.');
        
        % Get functions for the controller.
        ftarg = mpc.getCasadiFunc(@(x, u, Bd, dhat) ode(x, u, 0) + Bd*dhat, ...
                                  {Nx, Nu, [Nx, Nid], Nid}, ...
                                  {'x', 'u', 'Bd', 'dhat'}, ...
                                 'funcname', 'f');
        fmpc = mpc.getCasadiFunc(@(x, u, Bd, dhat) ode(x, u, 0) + Bd*dhat, ...
                                 {Nx, Nu, [Nx, Nid], Nid}, ...
                                 {'x', 'u', 'Bd', 'dhat'}, ...
                                 'rk4', true(), 'Delta', Delta, ...
                                 'funcname', 'f');
        fmhe = mpc.getCasadiFunc(@(x, u, Bd) [ode(x(1:Nx), u, 0) ...
                                              + Bd*x((Nx + 1):end);
                                              zeros(Nid, 1)], ...
                                 {Nx + Nid, Nu, [Nx, Nid]}, ...
                                 {'x', 'u', 'Bd'}, ...
                                 'rk4', true(), 'Delta', Delta, ...
                                 'funcname', 'faug');
        
        htarg = mpc.getCasadiFunc(@(C, x, Cd, dhat) C*x + Cd*dhat, ...
                                  {[Ny, Nx], Nx, [Ny, Nid], Nid}, ...
                                  {'C', 'x', 'Cd', 'dhat'}, ...
                                  'funcname', 'h');
        hmpc = htarg;
        hmhe = mpc.getCasadiFunc(@(C, x, Cd) C*x(1:Nx) + Cd*(x((Nx + 1):end)), ...
                                 {[Ny, Nx], Nx + Nid, [Ny, Nid]}, ...
                                 {'C', 'x', 'Cd'}, 'funcname', 'haug');
        
        e = mpc.getCasadiFunc(@slackoutputs, [Ny, Ns, Ny, Ny], ...
                              {'y', 's', 'ylb', 'yub'}, 'funcname', 'e');
        
        ltarg = mpc.getCasadiFunc(@targetcost, ...
            {Ny, Ny,   [Ny,Ny], Nu, Nu, [Nu,Nu],Ns, Ns}, ...
            {'y','ysp','Q',    'u','usp','R',  's','p'}, ...
            'funcname', 'ltarg');
        lmpc = mpc.getCasadiFunc(@stagecost, ...
            {Nx, Nx,   [Nx,Nx], Nu, Nu, [Nu,Nu], Nu,[Nu,Nu], Ns, Ns}, ...
            {'x','xsp','Q',    'u','usp','R',   'Du','S',    's','p'}, ...
            'funcname', 'l');
        lmhe = mpc.getCasadiFunc(@(w, Qinv, v, Rinv) w'*Qinv*w + v'*Rinv*v, ...
                                 {Nw, [Nw, Nw], Nv, [Nv, Nv]}, ...
                                 {'w', 'Qinv', 'v', 'Rinv'}, 'funcname', 'lmhe');
        
        Vf = mpc.getCasadiFunc(@(x, xsp, P) (x - xsp)'*P*(x - xsp), ...
                               {Nx, Nx, [Nx, Nx]}, {'x','xsp','P'}, ...
                               'funcname', 'Vf');
        
        lx = mpc.getCasadiFunc(@(x, xbar, Pinv) (x - xbar)'*Pinv*(x - xbar), ...
                               {Nx + Nid, Nx + Nid, (Nx + Nid)*[1, 1]}, ...
                               {'x', 'xbar', 'Pinv'}, 'funcname', 'lx');
        fprintf('.');
        
        % Assemble controller.
        N = struct('x', Nx, 'u', Nu, 'y', Ny, 's', Ns, 't', Nt);
        lb = struct('u', ulb, 'x', xmin);
        ub = struct('u', uub, 'x', xmax);
        guess = struct('u', repmat(mpcpar.usp, 1, Nt));
        controller = mpc.nmpc('f', fmpc, 'e', e, 'h', hmpc, 'l', lmpc, ...
                              'Vf', Vf, 'N', N, ...
                              'lb', lb, 'ub', ub, 'guess', guess, ...
                              'par', mpcpar, 'verbosity', 0);
        simcon.data.controller = controller;
        fprintf('.');
        
        % Assemble steady-state target finder.
        N = struct('x', Nx, 'u', Nu, 'y', Ny, 's', Ns);
        lb = struct('u', ulb, 'x', xmin);
        ub = struct('u', uub, 'x', xmax);
        guess = struct('u', mpcpar.usp);
        sstarg = mpc.sstarg('f', ftarg, 'e', e, 'h', htarg, 'l', ltarg, ...
                            'N', N, 'lb', lb, 'ub', ub, 'guess', guess, ...
                            'par', sstargpar, 'discretef', false(), ...
                            'verbosity', 0);
        simcon.data.sstarg = sstarg;
        fprintf('.');
        
        % Assemble moving horizon estimator. Disturbance model is output bias.
        N = struct('x', Nx + Nid, 'y', Ny, 'u', Nu, 't', Nmhe);
        lb = struct('x', [xmin; -inf(Nid, 1)]);
        ub = struct('x', [xmax; inf(Nid, 1)]);
        mhe = mpc.nmhe('f', fmhe, 'h', hmhe, 'l', lmhe, 'lx', lx, ...
                       'y', y, 'u', u, 'N', N, 'lb', lb, 'ub', ub, ...
                       'par', mhepar, 'verbosity', 0, 'wadditive', true());
        simcon.data.mhe = mhe;
        fprintf('.');
        
        % Get linear model.
        linmodel = struct('xlin', x_init, 'ulin', u_init, 'ylin', y_init);
        [linmodel.A, linmodel.B, linmodel.Bd] = mpc.getLinearizedModel(sim, ...
            {simcon.data.x_init, simcon.data.u_init, simcon.data.d_init}, ...
            {'A', 'B', 'Bd'}, 'deal', true());
        linmodel.C = C;
        simcon.data.linmodel = linmodel;    
        fprintf('done\n');
    end
    
    % Advance actual system with noise. It can't go through the ground.
    noise = simcon.Options.get('noisefactor');
    usys = usys + noise*randn(Nu, 1).*simcon.getvec('u', 'noise').*uscale;
    dsys = simcon.getvec('DVs', 'value') ...
           + noise*randn(Nd, 1).*simcon.getvec('d', 'noise').*dscale;
    if iteration > 0
        xsys = full(sim(xsys, usys, dsys));
        xsys = xsys + noise*randn(Nx, 1).*simcon.getvec('x', 'noise').*xscale;
        xsys = checkground(xsys);
    end
    ysys = C*xsys;
    ysys = ysys + noise*randn(Ny, 1).*simcon.getvec('y', 'noise').*yscale;
    simcon.data.y = [simcon.data.y(:,2:end), ysys];
    simcon.data.u = [simcon.data.u(:,2:end), usys];
    
    % Calculate and use Kalman Filter.
    Aaug = [linmodel.A, mhepar.Bd; zeros(Nid, Nx), eye(Nid)];
    Gaug = eye(Nx + Nid);
    Baug = [linmodel.B; zeros(Nid, Nu)];
    Caug = [linmodel.C, mhepar.Cd];
    Qaug = diag(1./diag(mhepar.Qinv));
    Raug = diag(1./diag(mhepar.Rinv));
    Raug(isinf(Raug)) = 1e12; % dlqe doesn't like infinite values here.
    [Laug, Paug] = dlqe(Aaug, Gaug, Caug, Qaug, Raug);
  
    if (iteration == 0)
        simcon.data.mheobjkm1 = 1e12;
    end
    
    if statefeedback
        % Just take actual state.
        xhat = xsys;
        dhat = zeros(Nid, 1);
        xmhe = NaN(Nx, Nmhe + 1);
        dmhe = NaN(Nid, Nmhe + 1);
    elseif nonlinmpc
        % Run MHE.
        mhepar.Pinv = simcon.Options.get('priorweight')*mpctools.spdinv(Paug);
        mhepar.y = simcon.data.y;
        mhepar.u = simcon.data.u;
        mhe.par = mhepar;
        mhe.lb.x = repmat([xmin; -inf(Nid, 1)], 1, Nmhe + 1);
        mhe.ub.x = repmat([xmax; inf(Nid, 1)], 1, Nmhe + 1);
        mheobjkm1 = simcon.data.mheobjkm1;
        for i = 1:5       
            mhe.solve();
            fprintf('  Estimator: %s (Obj: %g)\n', mhe.status, mhe.obj);
            mheobjrat = abs(mhe.obj/mheobjkm1);
            if mheobjrat < 1e5
                break
            end
            fprintf('  Restarting estimator\n'); 
            mhe.guess.x = zeros((Nx + Nid),(Nmhe + 1));
        end
        simcon.data.mheobjkm1 = mhe.obj;
        
        mhe.saveguess();
        
        xhat = mhe.var.x(1:Nx,end);
        dhat = mhe.var.x((Nx + 1):end,end);
        xmhe = mhe.var.x(1:Nx,:);
        dmhe = mhe.var.x((Nx + 1):end,:);
    else
        % Use Kalman filter.
        xhat = [simcon.getvec('x', 'estvalue'); dhat];
        xlin = [linmodel.xlin; zeros(Nid, 1)];
        ulin = linmodel.ulin;
        ylin = linmodel.ylin;
        xhatm = Aaug*(xhat - xlin) + Baug*(usys - ulin) + xlin;
        yhatm = Caug*(xhatm - xlin) + ylin;
        xhat = xhatm + Laug*(ysys - yhatm);
        
        dhat = xhat((Nx + 1):end);
        xhat = xhat(1:Nx);
        xmhe = NaN(Nx, Nmhe + 1);
        dmhe = NaN(Nid, Nmhe + 1);
    end
    simcon.history.xhat.pushpop(xhat);
    simcon.history.dhat.pushpop(dhat); 
    simcon.data.dhat = dhat;
    yhat = C*xhat + Cd*dhat;
    
    % Update plots.
    simcon.setvec('x', 'value', xsys);
    simcon.setvec('y', 'value', ysys);
    simcon.setvec('x', 'estvalue', xhat);
    simcon.setvec('y', 'estvalue', yhat);
    if isequal(dmodel{2}, 'd1')
        simcon.setvec('d', 'estvalue', dhat(2));
    end
    
    % Run controller and simulate predictions.
    multisim = @(x, u) checkground(rk4sim(@(x, u) ode(x, u, 0) + Bd*dhat, ...
                                          Delta, x, u));
    if simcon.isclosedloop
        % Steady-state target finder.
        if stale
            sstarg.guess.x = xsys;
        end
        hsp = simcon.CVs{1}.get('setpoint');
        sstarg.par = sstargpar;
        sstarg.par.ylb(1) = hsp;
        sstarg.par.yub(1) = hsp;
        sstarg.par.ysp(1) = hsp;
 %       dhat
        sstarg.par.dhat = dhat;
        sstarg.lb.u = ulb;
        sstarg.ub.u = uub;
        sstarg.solve();
        fprintf('  Target Finder: %s (Obj: %g)\n', sstarg.status, sstarg.obj);
        sstarg.saveguess();
        
        xtarg = sstarg.var.x;
        utarg = sstarg.var.u;
        
        % Controller.
        if stale
            controller.guess.x = repmat(xsys, 1, Nt + 1);
        end
        controller.par = mpcpar;
        controller.par.xsp = xtarg;
        controller.par.usp = utarg;
        controller.par.dhat = dhat;
        controller.lb.u = repmat(ulb, 1, Nt);
        controller.ub.u = repmat(uub, 1, Nt);
        controller.lb.Du = repmat(-dulim, 1, Nt);
        controller.ub.Du = repmat(dulim, 1, Nt);
        if nonlinmpc
            controller.fixvar('x', 1, xhat);
            controller.solve();
            fprintf('  Controller: %s (Obj: %g)\n', controller.status, ...
                    controller.obj);
            controller.saveguess();
            usys = controller.var.u(:,1);
            usim = controller.var.u;
            xsim = checkground(controller.var.x);
        else
            Aaug = blkdiag(linmodel.A, zeros(Nu, Nu));
            Baug = [linmodel.B; eye(Nu)];
            Qaug = blkdiag(mpcpar.Q, mpcpar.S);
            Raug = mpcpar.R + mpcpar.S;
            Raug(2,2) = Raug(2,2) + 1e3/uscale(2).^2; % Avoid using vent.
            Maug = [zeros(Nx, Nu); -mpcpar.S];
            Kaug = -dlqr(Aaug, Baug, Qaug, Raug, Maug);
            Kx = Kaug(:,1:Nx);
            Ku = Kaug(:,(Nx + 1):end);
            usim = NaN(Nu, Nt);
            xsim = NaN(Nx, Nt + 1);
            xsim(:,1) = xhat;
            uprev = mpcpar.uprev;
            for i = 1:Nt
                usim(:,i) = Kx*(xsim(:,i) - xtarg) + Ku*(uprev - utarg) + utarg;
                usim(:,i) = min(max(usim(:,i), ulb), uub);
                uprev = usim(:,i);
                xsim(:,i + 1) = linmodel.A*(xsim(:,i) - xtarg)...
                                + linmodel.B*(usim(:,i) - utarg) ...
                                + xtarg;
            end
            xsim = checkground(xsim);
            usys = usim(:,1);
        end
    else
        % Make forecast assuming constant input.
        usim = repmat(usys, 1, Nt);
        xsim = multisim(xhat, usim);
        
        xtarg = NaN(Nx, 1);
        utarg = NaN(Nu, 1);
    end
    
    % Apply quantization and re-simulate.
    if fuelincrement > 1e-3
        [usim, simcon.data.fuelerror] = quantizefuel(usim, ulb, uub, ...
                                                     fuelincrement, ...
                                                     simcon.data.fuelerror);
        usys = usim(:,1);
        if nonlinmpc
            xsim = multisim(xsim(:,1), usim);
        else
            for i = 1:Nt
                xsim(:,i + 1) = linmodel.A*(xsim(:,i) - xtarg)...
                                + linmodel.B*(usim(:,i) - utarg) ...
                                + Bd*dhat + xtarg;
            end
            xsim = checkground(xsim);
        end
    end
    simcon.setvec('u', 'value', usys);
    ysim = bsxfun(@plus, C*xsim, Cd*dhat);
    
    % Simulate constant-u system.
    uconst = repmat(usys, 1, Nt);
    xconst = multisim(xhat, uconst);
    yconst = bsxfun(@plus, C*xconst, Cd*dhat);
    
    % Update forecasts.
    simcon.setvec('x', 'setpoint', xtarg);
    simcon.setvec('u', 'setpoint', utarg);
    simcon.setpred('x', xsim);
    simcon.setpred('y', ysim);
    simcon.setpred('u', usim);
    simcon.setpred('yconstu', yconst);
    simcon.setpred('xmhe', xmhe, 'past');
    if isequal(dmodel{2}, 'd1')
        simcon.setpred('dmhe', dmhe(2,:), 'past');
    end
    
    % Done.
    fprintf('--------------------------------------------------\n');
end%function

