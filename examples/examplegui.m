function examplegui()
% Example gui for MPC simulation.
% create figure without a default toolbar

mvs = { ...
    mpcsim.MVObject('MV1', [], true(), 'pltmin', -10, 'pltmax', 110);
    mpcsim.MVObject('MV2');
};


xvs = { ...
    mpcsim.XVObject('XV1', 'Only XV', true());
};

cvs = { ...
    mpcsim.CVObject('CV1', 'First CV', true(), 'min', 0, 'max', 100);
    mpcsim.CVObject('CV2');
};

options = mpcsim.Option('Options', 'noisefactor', 0, ...
                        '.name', 'Noise Factor', '.chflag', true());

simcon = mpcsim.SimCon(@stepfunc, mvs, cvs, options, xvs);
simcon.mainloop();

end%function

function stepfunc(simcon, iteration, stale)
    % Simple step function just adding random noise.
    fprintf('Step %d\n', iteration);
    
    if stale
        fprintf('Updating object\n');
    end
    
    for i = 1:length(simcon.plotvars)
        v = simcon.plotvars{i};
        v.set('value', 100*rand(1));
    end

    % Change the constant-u predictions so we can see something happen.
    for i = 1:length(simcon.CVs)
        constu = simcon.CVs{i}.lines.constu;
        constu.y = simcon.CVs{i}.get('value')*ones(size(constu.y));
    end

    % Add some noise to the actual state value.
    for i = 1:length(simcon.XVs)
        simcon.XVs{i}.set('estvalue', simcon.XVs{i}.get('value') + 10*rand(1));
    end
end%function


