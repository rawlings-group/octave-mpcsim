function hab_open_loop()
% Simulation of hot-air balloon under manual control.  The balloon is a type
% AX7-77 from Head Balloons, Inc.
%
% This version uses continuous fuel valve and continuous vent (deflation port).
%

% Tom Badgwell 06/22/17

% Load packages
  
pkg('load', 'control');
mpc = import_mpctools();

% Set the time step and dimensions for the nonlinear system

Delta = 1.0; % sample time (dimensionless)
Nx    =   3; % number of states (altitude, vertical velocity, bag temperature)
Nu    =   2; % number of inputs (fuel valve position, vent valve position)
Ny    =   1; % number of ouputs (altitude, vertical velocity, bag temperature)

% Set scaling factors

hr = 1000  ; % altitude scale factor in m
tr = 10.10 ; % time scale factor in s
Tr = 288.2 ; % temperature scale factor in K
fr = 4870  ; % fuel valve postion scale factor in %
pr = 1485  ; % vent valve position scale factor in %

% Set parameters

pars        = struct();
pars.alpha  = 5.098;   % balloon number
pars.gamma  = 5.257;   % atmosphere number
pars.mu     = 0.1961;  % ratio payload weight to total weight
pars.omega  = 8.544;   % drag coefficient
pars.delta  = 0.0255;  % temperature drop-off coefficient
pars.beta   = 0.01683; % heat transfer coefficient
pars.fr     = fr;      % fuel valve position scale factor
pars.pr     = pr;      % vent valve position scale factor

% Define the ode describing the dynamics

function rhs = habode(x, u, pars)
  
    term1 = pars.alpha*pars.mu*(1 - pars.delta*x(1))^(pars.gamma -1);
    term2 = (1 - (1 - pars.delta*x(1))/x(3));
    term3 = (x(3) -1 + pars.delta*x(1));
    term4 = (pars.beta + u(2)/pars.pr);
    term5 = pars.omega*x(2)*abs(x(2));
    dx1dt = x(2); 
    dx2dt = term1*term2 - pars.mu - term5;
    dx3dt = -term3*term4 + u(1)/pars.fr;
    rhs = [dx1dt; dx2dt; dx3dt];

end
				
% Define casadi functions
	     
ode    = @(x, u) habode(x, u, pars);
habsim = mpc.getCasadiIntegrator(ode, Delta, [Nx, Nu], ...
                                  {'x', 'u'}, {'hab'});

% Choose simulation time and initial state

t0   = 0;
tf   = 5000; % seconds
Nsim = round((tf-t0)/Delta) + 1
xol(:,1) = [0.0;0.0;1.0];

% Set up the scenario

% Initial conditions

uol              = zeros(Nu, Nsim);
uol(1,   1: 250) =   0.0;
uol(2,   1: 250) =   0.0;

% Warm up the bag to neutral buoyancy

uol(1, 251: 750) =  20.0;
uol(2, 251: 750) =   0.;

% Takeoff

uol(1, 751:1250) =  25.0;
uol(2, 751:1250) =   0.0;

% Climb higher

uol(1,1251:1750) =  30.0;
uol(2,1251:1750) =   0.0;

% Open the vent

uol(1,1751:2250) =  30.0;
uol(2,1751:2250) =   5.0;

% Close the vent

uol(1,2251:2750) =  30.0;
uol(2,2251:2750) =   0.0;
  
% Descend

uol(1,2751:3250) =  22.0;
uol(2,2751:3250) =   0.0;
  
% Descend

uol(1,3251:3750) =  21.0;
uol(2,3251:3750) =   0.0;
  
% Land

uol(1,3751:4250) =  20.0;
uol(2,3751:4250) =   0.0;
  
% Land

uol(1,4251:Nsim) =   0.0;
uol(2,4251:Nsim) =   5.0;
  
% Loop for the simulation

for t = 1:Nsim

    fprintf('%10s %3d: \n', 'iteration ',t);

    xol(:,t + 1) = full(habsim(xol(:,t), uol(:,t)));
     
    % Clamp the states if on the ground

    if (xol(1,t + 1) <= 0)
      xol(1, t + 1) = 0;
      if (xol(2, t + 1) < 0)
        xol(2, t + 1) = 0;
      end
    end    
    
end

% Plot results in engineering units
 
xole = xol;
uole = uol;
for t = 1:(Nsim+1)
  xole(1,t) = hr*xol(1,t);
  xole(2,t) = (hr/tr)*xol(2,t);
  xole(3,t) = Tr*xol(3,t) - 273.2;
end
te  = (tr*Delta)*(1:Nsim+1)/60.0;

fig = figure();
plotargs = struct('fig', fig, 'marker', '');
mpc.mpcplot(xole(1:3,:), uole, te, 'color', 'g', 'xnames', {'h (m)', 'v (m/s)', 'T (C)'}, ...
            'unames', {'f (%)', 'v (%)'}, '**', plotargs);

% Send variables back to the base workspace.
vars = who();
for i = 1:length(vars)
    v = vars{i};
    assignin('base', v, eval(v));
end

end%function
