#!/usr/bin/env python
"""Read one or more bmp files and save RGB data to a mat file."""
import argparse
import os
import sys
import numpy as np
from scipy.io import savemat
from scipy.misc import imread

# Make argument parser.
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("-o", "--output", help="file name for output",
                    default="icons.mat")
parser.add_argument("bmp", help="bmp files to include", nargs="*")

def readbmp(bmpfile):
    """Reads a BMP file and returns it as a double array."""
    data = imread(bmpfile)
    scale = np.iinfo(data.dtype).max
    return data/float(scale)

def main(output, *bmpfiles):
    """Loads bmp files and saves to a mat file."""
    mat = {}
    for bmp in bmpfiles:
        [base, ext] = os.path.splitext(os.path.basename(bmp))
        if ext != ".bmp":
            raise ValueError("'{}' is not a bmp file!".format(bmp))
        mat[base] = readbmp(bmp)
    savemat(output, mat, appendmat=False)
        
if __name__ == "__main__":
    args = parser.parse_args(sys.argv[1:])
    main(args.output, *args.bmp)
