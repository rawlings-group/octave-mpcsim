#!/usr/bin/env python3
"""Adds all distribution files to a zip file for upload to Bitbucket."""

import argparse
import os
import sys
import zipfile

# Command-line arguments.
parser = argparse.ArgumentParser(description=__doc__, add_help=False)
parser.add_argument("--help", help="print this help", action="help")
parser.add_argument("--root", help="name for root folder in zip file")
parser.add_argument("--name", help="specify name for zip file",
                    default="mpctools.zip")
parser.add_argument("files", default=[], nargs="+",
                    help="Files to include")
kwargs = vars(parser.parse_args(sys.argv[1:]))

# Get files and name of zip file, and decide if there should be a root folder.
files = set(kwargs["files"])
zipname = kwargs["name"]
root = kwargs["root"]
if root is None:
    root = ""
  
# Decide whether to include README.
README = "README.md"
includereadme = (README in files)
if includereadme:
    files.remove(README)

# Now add files.
with zipfile.ZipFile(zipname, "w", zipfile.ZIP_DEFLATED) as distzip:
    # Recurse through VI directories.
    for fl in files:
        readfile = fl
        writefile = os.path.join(root, fl)
        distzip.write(readfile, writefile)
    
    # Also add readme with txt extension to play nice with Windows.
    if includereadme:
        distzip.write(README, os.path.join(root, os.path.splitext(README)[0]
                                               + ".txt"))
    print("Wrote zip file '%s'." % distzip.filename)
